/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  AY-3-8500 for MiSTer
//
//  Copyright (C) 2019 Cole Johnson
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================


`default_nettype none

module AY_3_8500_mc2
(
// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [4:1]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output wire [12:0] SDRAM_A,
	output wire  [1:0] SDRAM_BA,
	inout  wire [15:0] SDRAM_DQ,
	output wire        SDRAM_DQMH,
	output wire        SDRAM_DQML,
	output wire        SDRAM_CKE,
	output wire        SDRAM_nCS,
	output wire        SDRAM_nWE,
	output wire        SDRAM_nRAS,
	output wire        SDRAM_nCAS,
	output wire        SDRAM_CLK,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  = 1'bz,
	inout wire	ps2_mouse_data_io = 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output        AUDIO_L,
	output        AUDIO_R,
	input wire	ear_i,
	output wire	mic_o					= 1'b0,

		// VGA
	output  [4:0] VGA_R,
	output  [4:0] VGA_G,
	output  [4:0] VGA_B,
	output        VGA_HS,
	output        VGA_VS,

		// HDMI
	output wire	[7:0]tmds_o			= 8'b00000000,

		//STM32
	input wire	stm_tx_i,
	output wire	stm_rx_o,
	output wire	stm_rst_o			= 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
		
	inout wire	stm_b8_io, 
	inout wire	stm_b9_io,

	input         SPI_SCK,
	output        SPI_DO,
	input         SPI_DI,
	input         SPI_SS2
);

assign 	stm_rst_o			= 1'bz;

localparam CONF_STR = {
	//"O35,Scandoubler Fx,None,HQ2x,CRT 25%,CRT 50%,CRT 75%;",
	//"O6,Invisiball,OFF,ON;",
	"O7A,Color Pallette,Mono,Greyscale,RGB1,RGB2,Field,Ice,Christmas,Marksman,Las Vegas;",
	
	//"OB,Angle,Off,On;",
	"OC,Speed,Slow,Fast;",
	"OD,Size,Big,Small;",
	"OE,Auto Serve,Off,On;",
		 
	"O0,Reset;",
	"V,v2"
};
////////////////////   CLOCKS   ///////////////////

wire clk_sys, clk_vid, clk_off;
wire pll_locked, clock2M;

pll pll
(
	.inclk0(clock_50_i),
	//.c0(clk_sys), // 7.159mhz
	//.c1(clk_vid), // 28.636mhz
	
	.c0(clk_sys), // 50mhz
	.c1(clk_vid), // 25mhz
	
	.locked(pll_locked)
);

	reg [4:0] clockcount = 0;	
	reg [4:0] clockcount2 = 0;
	reg slow_clock = 0;
	wire tick4M = (clockcount < 12) ? 1'b1: 1'b0;
	always @(posedge clk_sys) begin
		if (clockcount == 24) begin
			clockcount <= 0;
		end else begin
			clockcount <= clockcount + 1;
		end
	end

	assign clock2M = tick4M;
	

///////////////////////IN+OUT///////////////////////

wire [31:0] status;
wire  [1:0] buttons;
wire        forced_scandoubler;


wire [10:0] ps2_key;

wire [15:0] joystick_0, joystick_1;
wire [15:0] joy = joystick_0;
wire [15:0] joy2 = joystick_1;
wire [15:0] joystick_analog_0;
wire [15:0] joystick_analog_1;








reg btnP1Up = 0;
reg btnP1Down = 0;
reg btnP2Up = 0;
reg btnP2Down = 0;
reg btnServe = 0;
reg btnReset = 0;
reg [7:0] gameBtns = 0;
reg [7:0] gameSelect = 7'b0000001;//Default to Tennis
reg btnAngle, btnAngleOld = 0;
reg btnSpeed, btnSpeedOld = 0;
reg btnSize, btnSizeOld = 0;
reg btnAutoserve, btnAutoserveOld = 0;
reg btnMiss = 0;
reg btnHit = 0;

reg [10:0] toggleInputs = 0;
reg angle = 0;
reg speed = 0;
reg size = 0;
reg autoserve = 0;
//Handle button toggling
always @(posedge clk_sys) begin
	btnAngleOld <= btnAngle;
	btnSpeedOld <= btnSpeed;
	btnSizeOld <= btnSize;
	btnAutoserveOld <= btnAutoserve;
	
	if(gameBtns[0:0])
		gameSelect = 7'b0000001;//Tennis
	else if(gameBtns[1:1])
		gameSelect = 7'b0000010;//Soccer
	else if(gameBtns[2:2])
		gameSelect = 7'b0000100;//Handicap (using a dummy bit)
	else if(gameBtns[3:3])
		gameSelect = 7'b0001000;//Squash
	else if(gameBtns[4:4])
		gameSelect = 7'b0010000;//Practice
	else if(gameBtns[5:5])
		gameSelect = 7'b0100000;//Rifle 1
	else if(gameBtns[6:6])
		gameSelect = 7'b1000000;//Rifle 2
		
	/*	
	if(btnAngle & !btnAngleOld)
		angle <= !angle;
	if(btnSpeed & !btnSpeedOld)
		speed <= !speed;
	if(btnSize & !btnSizeOld)
		size <= !size;
	if(btnAutoserve & !btnAutoserveOld)
		autoserve <= !autoserve;
		*/
		
		angle 		<= status[11];
		speed 		<= status[12];
		size 			<= status[13];
		autoserve 	<= status[14];
		
		
end
/////////////////Paddle Emulation//////////////////
wire [4:0] paddleMoveSpeed = speed ? 8 : 5;//Faster paddle movement when ball speed is high
reg [8:0] player1pos = 8'd128;
reg [8:0] player2pos = 8'd128;
reg [8:0] player1cap = 0;
reg [8:0] player2cap = 0;
reg hsOld = 0;
reg vsOld = 0;
always @(posedge clk_sys) begin
	hsOld <= hs;
	vsOld <= vs;
	if(vs & !vsOld) begin
		player1cap <= player1pos;
		player2cap <= player2pos;
		if(btnP1Up & player1pos>0)
			player1pos <= player1pos - paddleMoveSpeed;
		else if(btnP1Down & player1pos<8'hFF)
			player1pos <= player1pos + paddleMoveSpeed;
		if(btnP2Up & player2pos>0)
			player2pos <= player2pos - paddleMoveSpeed;
		else if(btnP2Down & player2pos < 8'hFF)
			player2pos <= player2pos + paddleMoveSpeed;
	end
	else if(hs & !hsOld) begin
		if(player1cap!=0)
			player1cap <= player1cap - 1;
		if(player2cap!=0)
			player2cap <= player2cap - 1;
	end
end
//Signal outputs (active-high except for sync)
wire audio;
wire rpOut;
wire lpOut;
wire ballOut;
wire scorefieldOut;
wire syncH;
wire syncV;
wire isBlanking;

//Misc pins
wire hitIn = (gameBtns[5:5] | gameBtns[6:6]) ? btnHit : audio;
//Still unknown why example schematic instructs connecting hitIn pin to audio during ball games
wire shotIn = (gameBtns[5:5] | gameBtns[6:6]) ? (btnHit | btnMiss) : 1;
wire lpIN = (player1cap == 0);
wire rpIN = (player2cap == 0);
wire lpIN_reset;//We don't use these signals, instead the VSYNC signal (identical) is directly accessed
wire rpIN_reset;
wire chipReset =  ~btn_n_i[1] | status[0];// | | btnReset;
ay38500NTSC the_chip
(
	.clk(clock2M),
	.superclock(clk_sys),
	.reset(!chipReset),
	.pinRPout(rpOut),
	.pinLPout(lpOut),
	.pinBallOut(ballOut),
	.pinSFout(scorefieldOut),
	.syncH(syncH),
	.syncV(syncV),
	.pinSound(audio),
	.pinManualServe(!(autoserve | btnServe)),
	.pinBallAngle(!angle),
	.pinBatSize(!size),
	.pinBallSpeed(!speed),
	.pinPractice(!gameSelect[4:4]),
	.pinSquash(!gameSelect[3:3]),
	.pinSoccer(!gameSelect[1:1]),
	.pinTennis(!gameSelect[0:0]),
	.pinRifle1(!gameSelect[5:5]),
	.pinRifle2(!gameSelect[6:6]),
	.pinHitIn(hitIn),
	.pinShotIn(shotIn),
	.pinLPin(lpIN),
	.pinRPin(rpIN)
);

/////////////////////VIDEO//////////////////////
wire hs = !syncH;
wire vs = !syncV;
wire hblank = !hs;
wire vblank = !vs;
wire [3:0] r,g,b;
wire showBall = !status[6:6] | (ballHide>0);
reg [5:0] ballHide = 0;
reg audioOld = 0;
always @(posedge clk_sys) begin
	audioOld <= audio;
	if(!audioOld & audio)
		ballHide <= 5'h1F;
	else if(vs & !vsOld & ballHide!=0)
		ballHide <= ballHide - 1;
end
reg [12:0] colorOut = 0;
always @(posedge clk_sys) begin
	if(ballOut & showBall) begin
		case(status[11:7])
			'h0: colorOut <= 12'hFFF;//Mono
			'h1: colorOut <= 12'hFFF;//Greyscale
			'h2: colorOut <= 12'hF00;//RGB1
			'h3: colorOut <= 12'hFFF;//RGB2
			'h4: colorOut <= 12'h000;//Field
			'h5: colorOut <= 12'h000;//Ice
			'h6: colorOut <= 12'hFFF;//Christmas
			'h7: colorOut <= 12'hFFF;//Marksman
			'h8: colorOut <= 12'hFF0;//Las Vegas
		endcase
	end
	else if(lpOut) begin
		case(status[11:7])
			'h0: colorOut <= 12'hFFF;//Mono
			'h1: colorOut <= 12'hFFF;//Greyscale
			'h2: colorOut <= 12'h0F0;//RGB1
			'h3: colorOut <= 12'h00F;//RGB2
			'h4: colorOut <= 12'hF00;//Field
			'h5: colorOut <= 12'hF00;//Ice
			'h6: colorOut <= 12'hF00;//Christmas
			'h7: colorOut <= 12'hFF0;//Marksman
			'h8: colorOut <= 12'hFF0;//Las Vegas
		endcase
	end
	else if(rpOut) begin
		case(status[11:7])
			'h0: colorOut <= 12'hFFF;//Mono
			'h1: colorOut <= 12'h000;//Greyscale
			'h2: colorOut <= 12'h0F0;//RGB1
			'h3: colorOut <= 12'hF00;//RGB2
			'h4: colorOut <= 12'h00F;//Field
			'h5: colorOut <= 12'h030;//Ice
			'h6: colorOut <= 12'h030;//Christmas
			'h7: colorOut <= 12'h000;//Marksman
			'h8: colorOut <= 12'hF0F;//Las Vegas
		endcase
	end
	else if(scorefieldOut) begin
		case(status[11:7])
			'h0: colorOut <= 12'hFFF;//Mono
			'h1: colorOut <= 12'hFFF;//Greyscale
			'h2: colorOut <= 12'h00F;//RGB1
			'h3: colorOut <= 12'h0F0;//RGB2
			'h4: colorOut <= 12'hFFF;//Field
			'h5: colorOut <= 12'h55F;//Ice
			'h6: colorOut <= 12'hFFF;//Christmas
			'h7: colorOut <= 12'hFFF;//Marksman
			'h8: colorOut <= 12'hF90;//Las Vegas
		endcase
	end
	else begin
		case(status[11:7])
			'h0: colorOut <= 12'h000;//Mono
			'h1: colorOut <= 12'h999;//Greyscale
			'h2: colorOut <= 12'h000;//RGB1
			'h3: colorOut <= 12'h000;//RGB2
			'h4: colorOut <= 12'h4F4;//Field
			'h5: colorOut <= 12'hCCF;//Ice
			'h6: colorOut <= 12'h000;//Christmas
			'h7: colorOut <= 12'h0D0;//Marksman
			'h8: colorOut <= 12'h000;//Las Vegas
		endcase
	end
end


//assign VGA_R = {colorOut[11:8], 1'b0};
//assign VGA_G = {colorOut[ 7:4], 1'b0};
//assign VGA_B = {colorOut[ 3:0], 1'b0};
//assign VGA_HS = hs;
//assign VGA_VS = vs;

wire [7:0] vga_r_s;	
wire [7:0] vga_g_s;	
wire [7:0] vga_b_s;	



osd  #(
	.STRLEN(($size(CONF_STR)>>3))
	) osd 
(
	.pclk(clk_sys),

	// SPI interface
	.sck ( SPI_SCK ),
	.ss  ( SPI_SS2 ),
	.sdi ( SPI_DI ),
	.sdo ( SPI_DO ),

	// VGA signals coming from core
	.red_in   ( {colorOut[11:8], 1'b0} ), 
	.green_in ( {colorOut[ 7:4], 1'b0} ),
	.blue_in  ( {colorOut[ 3:0], 1'b0} ),
	.hs_in ( hs ),
	.vs_in ( vs ),
	
	// VGA signals going to video connector
	.red_out    ( osd_r_s ),
	.green_out  ( osd_g_s ),
	.blue_out   ( osd_b_s ),
	//hs_out,
	//vs_out,
	
	//external data in to the microcontroller
	.data_in		 	( keys_s ),
	.conf_str		( CONF_STR ),
	.status			( status ),
	.menu_in		( 1'b0 ),
	
	.osd_enable(osd_enable),
	
	.reset (chipReset)
);


arcade_fx #(375, 12) arcade_video
(
	.clk_video(clk_vid),
	.ce_pix(clk_sys),
	
	.RGB_in({osd_r_s[4:1],osd_g_s[4:1],osd_b_s[4:1]}),
	.HBlank(hblank),
	.VBlank(vblank),
	.HSync(hs),
	.VSync(vs),
	
	.HDMI_R (vga_r_s ),
	.HDMI_G (vga_g_s ),
	.HDMI_B (vga_b_s ),
	.HDMI_HS(VGA_HS),
	.HDMI_VS(VGA_VS),
	//.HDMI_DE(nBlank),

	.forced_scandoubler(1),
	.fx(status[5:3])

);

wire [5:0] osd_r_s;	
wire [5:0] osd_g_s;	
wire [5:0] osd_b_s;	
wire nBlank = (VGA_HS & VGA_VS);



assign VGA_R = (nBlank) ? vga_r_s[7:3] : 5'd0;
assign VGA_G = (nBlank) ? vga_g_s[7:3] : 5'd0;
assign VGA_B = (nBlank) ? vga_b_s[7:3] : 5'd0;

////////////////////AUDIO////////////////////////
assign AUDIO_L = audio;//, 15'b0};
assign AUDIO_R = AUDIO_L;

always @(posedge clk_sys) 
begin
	 btnP2Up 		<= m_up; 
	 btnP2Down 		<= m_down; 
	 btnP1Up 		<= m_up2; 
	 btnP1Down		<= m_down2; 
	 btnServe 		<= m_fireA | m_fire2A | m_fireB | m_fire2B;
	 gameBtns[0:0] <= btn_1; // 1
	 gameBtns[1:1] <= btn_2; // 2
	 gameBtns[2:2] <= btn_3; // 3
	 gameBtns[3:3] <= btn_4; // 4
	 gameBtns[4:4] <= btn_5; // 5
	 gameBtns[5:5] <= btn_6; // 6
	 gameBtns[6:6] <= btn_7; // 7
	 btnAngle 		<= m_fireE; // Z
	 btnSpeed 		<= m_fireF; // X
	 btnSize 		<= m_fireG; // C
	 btnAutoserve  <= m_fireH; // V
	 btnMiss 		<= m_fireD; //L shift
	 btnHit 			<= m_tilt; // backspace
	// btnReset 		<= m_coin4; // ESC
end




//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG, m_fireH;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G, m_fire2H;
wire m_tilt, btn_5, btn_6, btn_7, m_coin4, btn_1, btn_2, btn_3, btn_4;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player = 	~btn_n_i[1] | btn_1;
wire btn_two_players = 	~btn_n_i[2] | btn_2;
wire btn_coin  = 			~btn_n_i[3] | btn_5;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD	( 3'b011 )) k_joystick
(
  .clk         	( clk_sys ),
  .kbdint      	( kbd_intr ),
  .kbdscancode 	( kbd_scancode ), 
  
	.joystick_0 	({ joy1_p6_i, joy1_p9_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
	.joystick_1		({ joy2_p6_i, joy2_p9_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
		  
	//-- joystick_0 and joystick_1 should be swapped
	.joyswap 		( 0 ),
		
	//-- player1 and player2 should get both joystick_0 and joystick_1
	.oneplayer		( 0 ),

	//-- tilt, coin4-1, start4-1
	.controls    ( {m_tilt, m_coin4, btn_7, btn_6, btn_5, btn_4, btn_3, btn_2, btn_1} ),
		
	//-- fire12-1, up, down, left, right

	.player1     ( {m_fireH, m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
	.player2     ( {m_fire2H, m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
		
	//-- keys to the OSD
	.sega_clk  		( hs ),
	.sega_strobe	( joyX_p7_o ),
		
	.osd_o		   ( keys_s ),
	.osd_enable 	( osd_enable ),
	
	//-- sega joystick
		
);

endmodule

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity OSD_Overlay is
port (
	clk : in std_logic;
	red_in : in std_logic_vector(7 downto 0);
	green_in : in std_logic_vector(7 downto 0);
	blue_in : in std_logic_vector(7 downto 0);
	window_in : in std_logic;
	hsync_in : in std_logic;
	osd_window_in : in std_logic;
	osd_pixel_in : in std_logic;
	red_out : out std_logic_vector(7 downto 0);
	green_out : out std_logic_vector(7 downto 0);
	blue_out : out std_logic_vector(7 downto 0);
	window_out : out std_logic;
	scanline_ena : in std_logic
);
end entity;

architecture RTL of OSD_Overlay is
	signal hsync_r : std_logic;
	signal scanline : std_logic :='0';
begin

	process(clk)
	begin
		if rising_edge(clk) then
			hsync_r<=hsync_in;
			if hsync_r='0' and hsync_in='1' then
				scanline<=not scanline;
			end if;
		end if;
	end process;

	process(clk, window_in, osd_window_in, osd_pixel_in,red_in, green_in, blue_in, scanline, scanline_ena)
	begin
	
--		if rising_edge(clk) then
			window_out<=window_in;
			
			if osd_window_in='1' then
				red_out<=osd_pixel_in&osd_pixel_in&red_in(7 downto 2);
				green_out<=osd_pixel_in&osd_pixel_in&green_in(7 downto 2);
				blue_out<=osd_pixel_in&'1'&blue_in(7 downto 2);
			elsif scanline='1' and scanline_ena='1' then
				red_out<='0'&red_in(7 downto 1);
				green_out<='0'&green_in(7 downto 1);
				blue_out<='0'&blue_in(7 downto 1);
			else
				red_out<=red_in;
				green_out<=green_in;
				blue_out<=blue_in;
			end if;
			
--		end if;
	
	end process;

end architecture;

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library STD;
use IEEE.NUMERIC_STD.ALL;


entity OBC1 is
	port(
		CLK			: in std_logic;
		RST_N			: in std_logic;
		ENABLE		: in std_logic;
		
		CA   			: in std_logic_vector(23 downto 0);
		DI				: in std_logic_vector(7 downto 0);
		CPURD_N		: in std_logic;
		CPUWR_N		: in std_logic;
		
		SYSCLKF_CE	: in std_logic;
		
		CS				: in std_logic;
						
		SRAM_A      : out std_logic_vector(12 downto 0);
		SRAM_DI		: in std_logic_vector(7 downto 0);
		SRAM_DO		: out std_logic_vector(7 downto 0)
	);
end OBC1;

architecture rtl of OBC1 is

	signal BASE			: std_logic;
	signal INDEX 		: std_logic_vector(6 downto 0);
	
	signal SRAM_ADDR 	: std_logic_vector(12 downto 0);
	
begin
		
	process( RST_N, CLK)
	begin
		if RST_N = '0' then
			INDEX <= (others => '0');
			BASE <= '0';
		elsif rising_edge(CLK) then
			if ENABLE = '1' and SYSCLKF_CE = '1' then
				if CPUWR_N = '0' and CS = '1' and CA(15 downto 4) = x"7FF" then 
					case CA(3 downto 0) is
						when x"5" =>
							BASE<= DI(0);
						when x"6" =>
							INDEX <= DI(6 downto 0);
						when others => null;
					end case; 
				end if;
			end if;
		end if;
	end process; 
	
	process( CA, BASE, INDEX )
	begin
		if CA(12 downto 3) = "1111111110" then	--7FF0-7FF7
			case CA(3 downto 0) is
				when x"0" | x"1" | x"2" | x"3" =>
					SRAM_ADDR <= "11" & not BASE & "0" & INDEX & CA(1 downto 0);
				when x"4" =>
					SRAM_ADDR <= "11" & not BASE & "1" & "0000" & INDEX(6 downto 2);
				when others =>
					SRAM_ADDR <= CA(12 downto 0);
			end case;
		else
			SRAM_ADDR <= CA(12 downto 0);
		end if;
	end process; 
	SRAM_A <= SRAM_ADDR;
	
	process( CA, DI, INDEX, SRAM_DI)
	begin
		if CA(12 downto 0) = "1111111110100" then	--7FF4
			case INDEX(1 downto 0) is
				when "00" =>
					SRAM_DO <= SRAM_DI(7 downto 2) & DI(1 downto 0);
				when "01" =>
					SRAM_DO <= SRAM_DI(7 downto 4) & DI(1 downto 0) & SRAM_DI(1 downto 0);
				when "10" =>
					SRAM_DO <= SRAM_DI(7 downto 6) & DI(1 downto 0) & SRAM_DI(3 downto 0);
				when others =>
					SRAM_DO <=                       DI(1 downto 0) & SRAM_DI(5 downto 0);
			end case;
		else
			SRAM_DO <= DI;
		end if;
	end process; 

end rtl;

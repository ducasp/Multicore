--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

package P65816_pkg is  

	type MicroInst_r is record
		stateCtrl    : std_logic_vector(2 downto 0);
		addrBus      : std_logic_vector(2 downto 0);
		addrInc      : std_logic_vector(1 downto 0);
		loadP			: std_logic_vector(2 downto 0); 
		loadT			: std_logic_vector(1 downto 0); 
		muxCtrl      : std_logic_vector(1 downto 0);
		addrCtrl     : std_logic_vector(7 downto 0);
		loadPC       : std_logic_vector(2 downto 0);
		loadSP       : std_logic_vector(2 downto 0);
		regAXY       : std_logic_vector(2 downto 0);
		loadDKB      : std_logic_vector(1 downto 0);
		busCtrl      : std_logic_vector(5 downto 0); 
		ALUCtrl      : std_logic_vector(4 downto 0); 
		byteSel      : std_logic_vector(1 downto 0);
		outBus       : std_logic_vector(2 downto 0);
		va      		 : std_logic_vector(1 downto 0);
	end record;
	
	type ALUCtrl_r is record
		fstOp        : std_logic_vector(2 downto 0);
		secOp        : std_logic_vector(2 downto 0);
		fc           : std_logic;
		w16          : std_logic;
	end record;
	
	type MCode_r is record
		ALU_CTRL 	: ALUCtrl_r;
		STATE_CTRL	: std_logic_vector(2 downto 0);
		ADDR_BUS      : std_logic_vector(2 downto 0);
		ADDR_INC      : std_logic_vector(1 downto 0);
		IND_CTRL      : std_logic_vector(1 downto 0);
		ADDR_CTRL     : std_logic_vector(7 downto 0);
		LOAD_PC       : std_logic_vector(2 downto 0);
		LOAD_SP       : std_logic_vector(2 downto 0);
		LOAD_AXY      : std_logic_vector(2 downto 0);
		LOAD_P         : std_logic_vector(2 downto 0);
		LOAD_T         : std_logic_vector(1 downto 0);
		LOAD_DKB      : std_logic_vector(1 downto 0);
		BUS_CTRL      : std_logic_vector(5 downto 0); 
		BYTE_SEL      : std_logic_vector(1 downto 0);
		OUT_BUS       : std_logic_vector(2 downto 0);
		VA      		 : std_logic_vector(1 downto 0);
	end record;
	
	type addrIncTab_t is array(0 to 3) of unsigned(15 downto 0);
	constant INC_TAB: addrIncTab_t := (x"0000", x"0001", x"0002", x"0003");
	
end P65816_pkg;

package body P65816_pkg is

	
end package body P65816_pkg;

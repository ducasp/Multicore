--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity Kbd_Joystick is
port (
  Clk          : in std_logic;
  KbdInt       : in std_logic;
  KbdScanCode  : in std_logic_vector(7 downto 0);
  JoyPCFRLDU   : out std_logic_vector(11 downto 0);
  osd_o         : out std_logic_vector(7 downto 0);
  direct_video    : out std_logic := '0'
);
end Kbd_Joystick;

architecture Behavioral of Kbd_Joystick is

signal IsReleased : std_logic;
signal osd_s        : std_logic_vector(7 downto 0) := (others=>'1');
signal direct_video_s : std_logic := '1';
signal btn_scroll : std_logic := '0';

begin 

osd_o <= osd_s;
direct_video <= direct_video_s;

process(btn_scroll)
begin                            
  if rising_edge(btn_scroll) then
        direct_video_s <= not direct_video_s;
  end if;
end process;

process(Clk)
begin
  if rising_edge(Clk) then
  
        if KbdInt = '1' then
            if KbdScanCode = "11110000" then IsReleased <= '1'; else IsReleased <= '0'; end if; 

            if KbdScanCode = "01110101" then JoyPCFRLDU(0) <= not(IsReleased); end if; -- up
            if KbdScanCode = "01110010" then JoyPCFRLDU(1) <= not(IsReleased); end if; -- down 
            if KbdScanCode = "01101011" then JoyPCFRLDU(2) <= not(IsReleased); end if; -- left
            if KbdScanCode = "01110100" then JoyPCFRLDU(3) <= not(IsReleased); end if; -- right

            if KbdScanCode = x"29"       then JoyPCFRLDU(4) <= not(IsReleased); end if; -- Space
            if KbdScanCode = x"11"       then JoyPCFRLDU(5) <= not(IsReleased); end if; -- alt
            if KbdScanCode = x"14"       then JoyPCFRLDU(6) <= not(IsReleased); end if; -- ctrl

            if KbdScanCode = x"1A"       then JoyPCFRLDU(7) <= not(IsReleased); end if; -- Z
            if KbdScanCode = x"22"       then JoyPCFRLDU(8) <= not(IsReleased); end if; -- X
            if KbdScanCode = x"21"       then JoyPCFRLDU(9)<= not(IsReleased); end if; -- C

            if KbdScanCode = x"59"       then JoyPCFRLDU(10) <= not(IsReleased); end if; -- r-shift
            if KbdScanCode = x"5A"      then JoyPCFRLDU(11)<= not(IsReleased); end if; -- ENTER

            -- OSD
            if KbdScanCode = "01110101" then osd_s(0) <= (IsReleased); end if; -- up    arrow : 0x75
            if KbdScanCode = "01110010" then osd_s(1) <= (IsReleased); end if; -- down  arrow : 0x72
            if KbdScanCode = "01101011" then osd_s(2) <= (IsReleased); end if; -- left  arrow : 0x6B
            if KbdScanCode = "01110100" then osd_s(3) <= (IsReleased); end if; -- right arrow : 0x74
            if KbdScanCode = x"5A"       then osd_s(4) <= (IsReleased); end if; -- ENTER 

            if KbdScanCode = x"07" and IsReleased = '0' then -- key F12
                osd_s(7 downto 5) <= "001"; -- OSD Menu command
            else
                osd_s(7 downto 5) <= "111"; -- release
            end if;

            if KbdScanCode = x"7E" then btn_scroll        <= not(IsReleased); end if; -- Scroll Lock

        end if;
 
  end if;
end process;

end Behavioral;



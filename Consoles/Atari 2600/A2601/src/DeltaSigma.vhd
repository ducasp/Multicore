--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- This is a Delta-Sigma Digital to Analog Converter
-- ported from Verilog to VHDL: Frank Buss, 2012

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- -----------------------------------------------------------------------

entity dac is
	generic(MSBI: integer := 4);
	port (
		clk : in std_logic;
		Reset : in std_logic;
		
		-- DAC input (excess 2**MSBI)		
		DACin : in std_logic_vector(MSBI downto 0);

		-- This is the average output that feeds low pass filter
		DACout : out std_logic
	);
end;

architecture rtl of dac is

	-- Output of Delta adder
	signal DeltaAdder : std_logic_vector(MSBI+2 downto 0);
	
	-- Output of Sigma adder
	signal SigmaAdder : std_logic_vector(MSBI+2 downto 0);

	-- Latches output of Sigma adder
	signal SigmaLatch : std_logic_vector(MSBI+2 downto 0);

	-- B input of Delta adder
	signal DeltaB : std_logic_vector(MSBI+2 downto 0);

begin

	process(clk, Reset)
	begin
		if Reset = '1' then
			SigmaLatch <= "1" & std_logic_vector(to_unsigned(0, MSBI + 2));
			DACout <= '1';
		else
			if rising_edge(clk) then
				SigmaLatch <= SigmaAdder;
				DACout <= SigmaLatch(MSBI + 2);
			end if;
		end if;
	end process;

	DeltaB <= SigmaLatch(MSBI + 2) & SigmaLatch(MSBI + 2) & std_logic_vector(to_unsigned(0, MSBI + 1));
	DeltaAdder <= std_logic_vector(unsigned(DACin) + unsigned(DeltaB));
	SigmaAdder <= std_logic_vector(unsigned(DeltaAdder) + unsigned(SigmaLatch));

end;

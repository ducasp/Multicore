--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- FPGA Colecovision
--
-- $Id: cv_console-c.vhd,v 1.3 2006/01/05 22:25:25 arnim Exp $
--
-------------------------------------------------------------------------------

configuration cv_console_struct_c0 of cv_console is

  for struct

    for por_b: cv_por
      use configuration work.cv_por_rtl_c0;
    end for;

    for clock_b: cv_clock
      use configuration work.cv_clock_rtl_c0;
    end for;

    for t80a_b: T80a
      use configuration work.T80a_rtl_c0;
    end for;

    for vdp18_b: vdp18_core
      use configuration work.vdp18_core_struct_c0;
    end for;

    for psg_b: sn76489_top
      use configuration work.sn76489_top_struct_c0;
    end for;

    for ctrl_b: cv_ctrl
      use configuration work.cv_ctrl_rtl_c0;
    end for;

    for addr_dec_b: cv_addr_dec
      use configuration work.cv_addr_dec_rtl_c0;
    end for;

    for bus_mux_b: cv_bus_mux
      use configuration work.cv_bus_mux_rtl_c0;
    end for;

  end for;

end cv_console_struct_c0;

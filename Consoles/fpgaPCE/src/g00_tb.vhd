--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity g00_tb is
end g00_tb;

architecture sim of g00_tb is

signal LDATA 	: std_logic_vector(23 downto 0);
signal RDATA	: std_logic_vector(23 downto 0);

signal CLK		: std_logic;
signal RST		: std_logic;
signal INIT		: std_logic;
signal WE		: std_logic;

signal PULSE_48KHZ	: std_logic;
signal AUD_MCLK		: std_logic;
signal AUD_BCLK		: std_logic;
signal AUD_DACDAT	: std_logic;
signal AUD_DACLRCK	: std_logic;
signal I2C_SDAT		: std_logic;
signal I2C_SCLK		: std_logic;

begin

g00 : entity work.g00_audio_interface
port map(
	LDATA	=> LDATA,
	RDATA	=> RDATA,     
	clk		=> CLK, 
	rst 	=> RST,
	INIT	=> INIT,
	W_EN	=> WE,
	pulse_48KHz	=> PULSE_48KHZ,
	AUD_MCLK	=> AUD_MCLK,
	AUD_BCLK	=> AUD_BCLK,
	AUD_DACDAT	=> AUD_DACDAT,
	AUD_DACLRCK	=> AUD_DACLRCK,
	I2C_SDAT	=> I2C_SDAT,
	I2C_SCLK	=> I2C_SCLK
);


LDATA <= (others => '0');
RDATA <= (others => '1');

-- CLOCK (24 MHz)
process
begin
	CLK <= '0';
	wait for 20.833 ns;
	CLK <= '1';
	wait for 20.833 ns;
end process;

-- RESET
process
begin
	RST <= '1';
	wait for 100 ns;
	RST <= '0';
	wait;
end process;

-- INIT
process
begin
	INIT <= '0';
	wait for 150 ns;
	INIT <= '1';
	wait;
end process;

WE <= '0';

end sim;
--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.target_pkg.all;
use work.project_pkg.all;

package platform_pkg is

	--  
	-- PACE constants which *MUST* be defined
	--
	
	constant PACE_INPUTS_NUM_BYTES        : integer := 9;
	
  -- depends on build or simulation
  constant COCO1_SOURCE_ROOT_DIR  : string := "../../src/plataform/";
  constant COCO1_ROM_DIR          : string := COCO1_SOURCE_ROOT_DIR & "roms/";

	--
	-- Platform-specific constants (optional)
	--

  type from_PLATFORM_IO_t is record
    -- to connect to real 6809
    cpu_6809_r_wn     : std_logic;
    cpu_6809_busy     : std_logic;
    cpu_6809_lic      : std_logic;
    cpu_6809_vma      : std_logic;
    cpu_6809_a        : std_logic_vector(15 downto 0);
    cpu_6809_d_o      : std_logic_vector(7 downto 0);
    -- from the OCIDE core
    wb_ack      : std_logic;
    wb_dat      : std_logic_vector(31 downto 0);
    wb_inta     : std_logic;
  end record;

  type to_PLATFORM_IO_t is record
    arst              : std_logic;
    clk_cpld          : std_logic;
    -- to connect to real 6809
    cpu_6809_q        : std_logic;
    cpu_6809_e        : std_logic;
    cpu_6809_rst_n    : std_logic;
    cpu_6809_d_i      : std_logic_vector(7 downto 0);
    cpu_6809_halt_n   : std_logic;
    cpu_6809_irq_n    : std_logic;
    cpu_6809_firq_n   : std_logic;
    cpu_6809_nmi_n    : std_logic;
    cpu_6809_tsc      : std_logic;
    -- to display on 7-segment display
    seg7              : std_logic_vector(15 downto 0);
    -- to the OCIDE core
    wb_clk      : std_logic;
    wb_arst_n   : std_logic;
    wb_rst      : std_logic;
    wb_cyc_stb  : std_logic;
    wb_adr      : std_logic_vector(6 downto 2);
    wb_dat      : std_logic_vector(31 downto 0);
    wb_we       : std_logic;
    -- to the SD core
    clk_50M     : std_logic;
  end record;

end;

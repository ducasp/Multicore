onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb/reset_s
add wave -noupdate /tb/clock_s
add wave -noupdate /tb/modsel_s
add wave -noupdate /tb/enaltset_s
add wave -noupdate -radix hexadecimal /tb/vram_d_s
add wave -noupdate -radix hexadecimal /tb/vram_a_s
add wave -noupdate /tb/video_bit_s
add wave -noupdate /tb/video_hs_n_s
add wave -noupdate /tb/video_vs_n_s
add wave -noupdate -radix unsigned /tb/u_target/hcount_q
add wave -noupdate -radix unsigned /tb/u_target/vcount_q
add wave -noupdate -radix unsigned /tb/u_target/char_row_q
add wave -noupdate -radix unsigned /tb/pix_hcnt_s
add wave -noupdate -radix unsigned /tb/pix_vcnt_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {65750 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {19990 us} {20022 us}

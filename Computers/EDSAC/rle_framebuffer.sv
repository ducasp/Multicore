/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module rle_framebuffer
(
   input             clock,
   input             enable,
   input             sync,
   input [1:0]       active_mode,
   input [31:0]      status,
   output reg [7:0]  pixel,
   
   // Option to upload/change the ROM image file //   
   input [7:0]       data,
   input [16:0]      wraddress,
   input             wren,

   output [18:0] sram_addr,
   inout  [ 7:0] sram_data,
   output        sram_we
);

/* 
   This works by decompressing the provided ROM bitmap using a relatively simple RLE variant:
   Load byte. If LSB bit is 0, display remaining 7 bits as luma and proceed to the next byte.
   If LSB bit is 1, then use remaining 7 bits as luma but they will be repeated. Now we need to
   load repeat count. Keep loading bytes and extracting 7 bits from each of them until we get 
   one with LSB set to 0, that is the end of SIZE data. Save that size data into a counter and
   decrement with every posedge clock, keeping the original 7 bits for pixel luma in the output
   register. After counter drops to zero, restart the entire process.
*/

wire [7:0] fp_rom_data;
wire rom_clock = clock & enable;

parameter
    FETCH     = 2'b00,
    GET_LEN   = 2'b01,
    COPY_CHAR = 2'b10;

/*
image_library fp_rom(
   .rdaddress({active_mode[1], rom_addr}),
   .wraddress(wraddress),
   .data(data),
   .wren(wren),
   .clock(rom_clock),
   .q(fp_rom_data)
);
*/

always @(posedge clock)
begin
   if (rom_clock) begin
      fp_rom_data <= sram_data;
   end
end


assign sram_addr = (wren) ? {2'b00,wraddress} : {2'b00, active_mode[1], rom_addr};
assign sram_data = (wren) ? data : 8'hzz;
assign sram_we = ~wren;


reg [21:0] repeat_count = 22'd0;
reg [15:0] rom_addr = 16'b0;
reg [1:0] phase = 2'd0;
reg [2:0] size_len = 3'd0;

reg old_sync;

always @(posedge clock)
begin
   
   if (sync) begin
      rom_addr <= 16'hfffe;
      phase <= FETCH;
   end
   
   else
   if (enable)
   begin
   
   rom_addr <= rom_addr + 1'b1;
   
   case (phase)
      FETCH:   // Get pixel value
         begin
            repeat_count <= 22'b0;
            pixel <= {fp_rom_data[7:1], 1'b0};
            size_len <= {2'b0, fp_rom_data[0]};
            
            if (fp_rom_data[0])              // This pixel is encoded, let's get the repeat count
               phase <= GET_LEN;
         end
         
         
      GET_LEN: // Get up to 3 bytes of size
         begin                      
            
            // When we reach the end of size data, subtract the number of clocks spent fetching the size
            repeat_count <= {repeat_count[14:0], fp_rom_data[7:1]};
            size_len <= size_len + 1'b1;     // Keep tabs on clock count
            
            // There is more size data to come                          
            if (~fp_rom_data[0]) 
            begin
               rom_addr <= rom_addr - {repeat_count[8:0], fp_rom_data[7:1]} + {14'b0, size_len} + 16'd2;             
               phase <= COPY_CHAR;
            end
         end
         
         
      COPY_CHAR:  // Churn out pixels
         begin
            // While repeat_count is not zero
            if(repeat_count > {19'b0, size_len} + 22'd1)
               repeat_count <= repeat_count - 1'b1;            
            else begin
               phase <= FETCH;
            end
         end      
         
      default:
         phase <= FETCH;
   endcase
   end
end

endmodule
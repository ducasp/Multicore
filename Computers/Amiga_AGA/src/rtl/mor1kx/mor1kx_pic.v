/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: mor1kx PIC

  Copyright (C) 2012 Authors

  Author(s): Julius Baxter <juliusbaxter@gmail.com>

***************************************************************************** */

`include "mor1kx-defines.v"

module mor1kx_pic
  (/*AUTOARG*/
   // Outputs
   spr_picmr_o, spr_picsr_o, spr_bus_ack, spr_dat_o,
   // Inputs
   clk, rst, irq_i, spr_access_i, spr_we_i, spr_addr_i, spr_dat_i
   );

   parameter OPTION_PIC_TRIGGER="LEVEL";
   parameter OPTION_PIC_NMI_WIDTH = 0;

   input clk;
   input rst;

   input [31:0] irq_i;

   output [31:0] spr_picmr_o;
   output [31:0] spr_picsr_o;

   // SPR Bus interface
   input         spr_access_i;
   input         spr_we_i;
   input [15:0]  spr_addr_i;
   input [31:0]  spr_dat_i;
   output        spr_bus_ack;
   output [31:0] spr_dat_o;

   // Registers
   reg [31:0]    spr_picmr;
   reg [31:0]    spr_picsr;

   wire spr_picmr_access;
   wire spr_picsr_access;

   wire [31:0]   irq_unmasked;

   assign spr_picmr_o = spr_picmr;
   assign spr_picsr_o = spr_picsr;

   assign spr_picmr_access =
     spr_access_i &
     (`SPR_OFFSET(spr_addr_i) == `SPR_OFFSET(`OR1K_SPR_PICMR_ADDR));
   assign spr_picsr_access =
     spr_access_i &
     (`SPR_OFFSET(spr_addr_i) == `SPR_OFFSET(`OR1K_SPR_PICSR_ADDR));

   assign spr_bus_ack = spr_access_i;
   assign spr_dat_o =  (spr_access_i & spr_picsr_access) ? spr_picsr :
                       (spr_access_i & spr_picmr_access) ? spr_picmr :
                       0;

   assign irq_unmasked = spr_picmr & irq_i;

   generate

      genvar 	 irqline;

      if (OPTION_PIC_TRIGGER=="EDGE") begin : edge_triggered
	 for(irqline=0;irqline<32;irqline=irqline+1)
	   begin: picgenerate
	      // PIC status register
	      always @(posedge clk `OR_ASYNC_RST)
		if (rst)
		  spr_picsr[irqline] <= 0;
	      // Clear
		else if (spr_we_i & spr_picsr_access)
		  spr_picsr[irqline] <= spr_dat_i[irqline] ? 0 :
					       spr_picsr[irqline];
	      // Set
		else if (!spr_picsr[irqline] & irq_unmasked[irqline])
		  spr_picsr[irqline] <= 1;
	   end // block: picgenerate
      end // if (OPTION_PIC_TRIGGER=="EDGE")

      else if (OPTION_PIC_TRIGGER=="LEVEL") begin : level_triggered
         for(irqline=0;irqline<32;irqline=irqline+1)
           begin: picsrlevelgenerate
              // PIC status register
              always @(*)
                spr_picsr[irqline] <= irq_unmasked[irqline];
           end
      end // if (OPTION_PIC_TRIGGER=="LEVEL")

      else if (OPTION_PIC_TRIGGER=="LATCHED_LEVEL") begin : latched_level
	 for(irqline=0;irqline<32;irqline=irqline+1)
	   begin: piclatchedlevelgenerate
	      // PIC status register
	      always @(posedge clk `OR_ASYNC_RST)
		if (rst)
		  spr_picsr[irqline] <= 0;
		else if (spr_we_i && spr_picsr_access)
		  spr_picsr[irqline] <= irq_unmasked[irqline] |
					       spr_dat_i[irqline];
		else
		  spr_picsr[irqline] <= spr_picsr[irqline] |
					irq_unmasked[irqline];
	   end // block: picgenerate
      end // if (OPTION_PIC_TRIGGER=="EDGE")

      else begin : invalid
	 initial begin
	    $display("Error - invalid PIC level detection option %s",
		     OPTION_PIC_TRIGGER);
	    $finish;
	 end
      end // else: !if(OPTION_PIC_TRIGGER=="LEVEL")
   endgenerate

   // PIC (un)mask register
   always @(posedge clk `OR_ASYNC_RST)
     if (rst)
       spr_picmr <= {{(32-OPTION_PIC_NMI_WIDTH){1'b0}},
		     {OPTION_PIC_NMI_WIDTH{1'b1}}};
     else if (spr_we_i && spr_picmr_access)
       spr_picmr <= {spr_dat_i[31:OPTION_PIC_NMI_WIDTH],
		     {OPTION_PIC_NMI_WIDTH{1'b1}}};

endmodule // mor1kx_pic

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--
-- HT 1080Z (TSR-80 clone) top level
--
--
-- Copyright (c) 2016-2017 Jozsef Laszlo (rbendr@gmail.com)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--

------------------------------------------------------------------------------
--
--  Multicore 2 Top by Victor Trucco
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL; 
use work.mist.ALL;

entity ht1080z is
    Port ( 
	  -- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		SDRAM_A			: out std_logic_vector(12 downto 0);
		SDRAM_DQ			: inout std_logic_vector(15 downto 0);

		SDRAM_BA			: out std_logic_vector(1 downto 0);
		SDRAM_DQMH			: out std_logic;
		SDRAM_DQML			: out std_logic;
		
		SDRAM_nRAS			: out std_logic;
		SDRAM_nCAS			: out std_logic;
		SDRAM_CKE			: out std_logic;
		SDRAM_CLK			: out std_logic;
		SDRAM_nCS			: out std_logic;
		SDRAM_nWE			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= 'Z';
		sd_sclk_o			: out   std_logic								:= 'Z';
		sd_mosi_o			: out   std_logic								:= 'Z';
		sd_miso_i			: in    std_logic								:= 'Z';

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		AUDIO_L				: out   std_logic								:= '0';
		AUDIO_R				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		VGA_R				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		VGA_G				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		VGA_B				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		VGA_HS		: out   std_logic								:= '1';
		VGA_VS		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';

		
		SPI_SCK			: in std_logic		:= 'Z';
		SPI_DO			: out std_logic		:= 'Z';
		SPI_DI			: in std_logic		:= 'Z';
		SPI_SS2			: in std_logic		:= 'Z';

		SPI_nWAIT		: out std_logic := '1'
			  ); 
end ht1080z;

architecture Behavioral of ht1080z is

component data_io
generic
		(
			STRLEN 		 : integer := 0
		);
  port ( 
			sck, ss, sdi 	:	in std_logic;
			sdo 				:  out std_logic;
			
			data_in 	: in std_logic_vector(7 downto 0);
			conf_str : in std_logic_vector( (STRLEN * 8)-1 downto 0);

			status 	: out std_logic_vector(31 downto 0);
			
			-- download info
			downloading  	:  out std_logic;
			--size				:  out std_logic_vector(24 downto 0);
			index				:  out std_logic_vector(4 downto 0);
  
			-- external ram interface
			clk				:	in std_logic;
			wr					:  out std_logic;
			addr   			:  out std_logic_vector(24 downto 0);
			data				:  out std_logic_vector(7 downto 0)
);
end component data_io;

component sdram is
      port( sd_data : inout std_logic_vector(15 downto 0);
            sd_addr : out std_logic_vector(12 downto 0);
             sd_dqm : out std_logic_vector(1 downto 0);
              sd_ba : out std_logic_vector(1 downto 0);
              sd_cs : out std_logic;
              sd_we : out std_logic;
             sd_ras : out std_logic;
             sd_cas : out std_logic;
               init : in std_logic;
                clk : in std_logic;
             clkref : in std_logic;
                din : in std_logic_vector(7 downto 0);
               dout : out std_logic_vector(7 downto 0);
               addr : in std_logic_vector(24 downto 0);
                 oe : in std_logic;
                 we : in std_logic
      );
end component;

constant CONF_STR : string := "P,HT1080Z.dat;S1,CAS,Load *.CAS;O12,Scanlines,Off,25%,50%,75%;O3,Video,PAL,NTSC;T6,Reset";

  function to_slv(s: string) return std_logic_vector is
    constant ss: string(1 to s'length) := s;
    variable rval: std_logic_vector(1 to 8 * s'length);
    variable p: integer;
    variable c: integer;
  
  begin
    for i in ss'range loop
      p := 8 * i;
      c := character'pos(ss(i));
      rval(p - 7 to p) := std_logic_vector(to_unsigned(c,8));
    end loop;
    return rval;

  end function;   


signal sdram_dqm  : std_logic_vector(1 downto 0);
signal ram_addr : std_logic_vector(24 downto 0);
signal  ram_din : STD_LOGIC_VECTOR(7 downto 0);
signal ram_dout : STD_LOGIC_VECTOR(7 downto 0);
signal ram_we: std_logic;
signal ram_oe: std_logic; 

signal   dn_go : std_logic;
signal   dn_wr : std_logic;
signal dn_addr : std_logic_vector(24 downto 0);
signal dn_data : std_logic_vector(7 downto 0);
signal  dn_idx : std_logic_vector(4 downto 0);

signal   dn_wr_r : std_logic;
signal dn_wr_new : std_logic;
signal dn_addr_r : std_logic_vector(24 downto 0);
signal dn_data_r : std_logic_vector(7 downto 0);

signal res_cnt : std_logic_vector(5 downto 0) := "111111";
signal autores : std_logic; 

  
signal scandoubler_disable : std_logic;
signal ypbpr : std_logic;
signal buttons : std_logic_vector(1 downto 0);

signal PS2CLK : std_logic;
signal PS2DAT : std_logic;

signal MPS2CLK : std_logic;
signal MPS2DAT : std_logic;

signal joy0 : std_logic_vector(31 downto 0);
signal joy1 : std_logic_vector(31 downto 0);

signal status: std_logic_vector(31 downto 0);
  
signal clk42m : std_logic;
signal pllLocked : std_logic;
 
signal cpua     : std_logic_vector(15 downto 0); 
signal cpudo    : std_logic_vector(7 downto 0);
signal cpudi    : std_logic_vector(7 downto 0);
signal cpuwr,cpurd,cpumreq,cpuiorq,cpunmi,cpuint,cpum1,cpuClkEn,clkref : std_logic;

signal rgbi : std_logic_vector(3 downto 0);
signal hs,vs : std_logic;
signal romdo,vramdo,ramdo,ramHdo,kbdout : std_logic_vector(7 downto 0);
signal vramcs : std_logic;

signal page,vcut,swres : std_logic;

signal romrd,ramrd,ramwr,vramsel,kbdsel : std_logic;
signal ior,iow,memr,memw : std_logic;
signal vdata : std_logic_vector(7 downto 0);

signal clk42div : std_logic_vector(4 downto 0);

signal dacout : std_logic;
signal sndBC1,sndBDIR,sndCLK : std_logic;
signal oaudio,snddo : std_logic_vector(7 downto 0); 

signal ht_rgb : std_logic_vector(17 downto 0);
signal p_hs,p_vs,vgahs,vgavs : std_logic; 
signal pclk : std_logic; 

signal io_ram_addr : std_logic_vector(23 downto 0);
signal iorrd,iorrd_r : std_logic;

signal audiomix : std_logic_vector(8 downto 0); 
signal tapebits : std_logic_vector(2 downto 0); 
signal  speaker : std_logic_vector(7 downto 0); 
signal vga : std_logic := '0';
signal oddline : std_logic;

signal inkpulse, paperpulse, borderpulse : std_logic;

-- OSD
signal pump_active_s : std_logic := '0';
signal osd_s  		 	: std_logic_vector(7 downto 0) := "00111111";
signal keys_s			: std_logic_vector( 7 downto 0) := (others => '1');	
signal power_on_reset: std_logic := '0';
signal power_on_s		: std_logic_vector(15 downto 0)	:= (others => '1');

signal clk_keyb : std_logic := '0';
signal osd_enable : std_logic := '0';
			
begin

	stm_rst_o	<= 'Z'; 

  --led <= not dn_go;--swres;
  
  -- generate system clocks 
  clkmgr : entity work.pll
   port map (
	  inclk0 => clock_50_i,
	  c0 => clk42m,
	  locked => pllLocked
	); 

	SDRAM_CLK <= clk42m;

  process(clk42m)
  begin
    if rising_edge(clk42m) then
	    clk42div <= clk42div + 1;
		 
	--	 clk_keyb <= not clk_keyb;
		 
		 -- 42 MHz/24 = 1.75 MHz
		 if clk42div = 23 then clk42div <= (others => '0'); end if;
		 
	 end if;
  end process;
  
  ior <= cpurd or cpuiorq or (not cpum1);
  iow <= cpuwr or cpuiorq;
  memr <= cpurd or cpumreq;
  memw <= cpuwr or cpumreq;
  
  romrd <= '1' when memr='0' and cpua<x"3780" else '0';
  ramrd <= '1' when cpua(15 downto 14)="01" and memr='0' else '0';
  ramwr <= '1' when cpua(15 downto 14)="01" and memw='0' else '0';
  vramsel <= '1' when cpua(15 downto 10)="001111" and cpumreq='0' else '0';
  kbdsel  <= '1' when cpua(15 downto 10)="001110" and memr='0' else '0';
  iorrd <= '1' when ior='0' and cpua(7 downto 0)=x"04" else '0'; -- in 04
    
  cpu : entity work.T80s
   port map (  
	   RESET_n => autores, --swres,
		CLK     => clk42m, 
		CEN     => cpuClkEn and not dn_go, -- 1.75 MHz
		WAIT_n  => '1',
		INT_n   => '1',
		NMI_n   => '1',
		BUSRQ_n => '1',
		M1_n    => cpum1,
		MREQ_n  => cpumreq,
		IORQ_n  => cpuiorq,
		RD_n    => cpurd,
		WR_n    => cpuwr,
		RFSH_n  => open,
		HALT_n  => open,
		BUSAK_n => open,
		A       => cpua, 
		DI      => cpudi,
		DO      => cpudo
  );

  cpudi <= --romdo when romrd='1' else
	        --ramdo when ramrd='1' else
			  --ram_dout when romrd='1' else
			  --ram_dout when ramrd='1' else
			  vramdo when vramsel='1' else
			  kbdout when kbdsel='1' else
			  x"30" when ior='0' and cpua(7 downto 0)=x"fd" else -- printer io read
			  --ram_dout when iorrd='1' else
			  --x"ff";
			  ram_dout;


  vdata <= cpudo when cpudo>x"1f" else cpudo or x"40";
  -- video ram at 0x3C00
  video : entity work.videoctrl 
    port map (   
	  reset => autores, --swres and pllLocked,
	  clk42 => clk42m,
			a => cpua(13 downto 0),
		 din => vdata,--cpudo,
		dout => vramdo,
		mreq => cpumreq,
		iorq => cpuiorq,
		  wr => cpuwr,
		  cs => not vramsel,
		hz60 => status(3),
		vcut => vcut,
		vvga => '0',
		page => page,
		rgbi => rgbi, 
	   pclk => pclk,
		inkp => inkpulse,
	 paperp => paperpulse,
	borderp => borderpulse,
	oddline => oddline,
	  hsync => hs,
	  vsync => vs
   );  

  kbd : entity work.ps2kbd 
    port  map ( 
	   RESET => not pllLocked,
 	   KBCLK => ps2_clk_io,
	   KBDAT => ps2_data_io,
		SWRES => swres,
	     CLK => clkref, --clk_keyb,
		    A => cpua(7 downto 0),
		 DOUT => kbdout,
		 PAGE => page,
		 VCUT => vcut,
		 INKP => inkpulse,
	  PAPERP => paperpulse,
	 BORDERP => borderpulse,
	 keys_o  => keys_s,
	 osd_enable => osd_enable

		  );
	
	-- PSG 
	-- out 1e = data port
	-- out 1f = register index

 soundchip : entity work.YM2149 
   port map (
  -- data bus
  I_DA      => cpudo,     
  O_DA      => open,     
  O_DA_OE_L => open,     
  -- control
  I_A9_L   	=> '0',      
  I_A8      => '1',     
  I_BDIR    => sndBDIR,     
  I_BC2     => '1',     
  I_BC1     => sndBC1,     
  I_SEL_L   => '1',     

  O_AUDIO   => oaudio,     
  -- port a
  I_IOA      => "ZZZZZZZZ",     
  O_IOA      => open,     
  O_IOA_OE_L => open,         
  -- port b
  I_IOB      => "ZZZZZZZZ",         
  O_IOB      => open,         
  O_IOB_OE_L => open,         
  --
  ENA        => cpuClkEn,
  RESET_L    => autores,--swres and pllLocked,    
  CLK        => clk42m
  );
  sndBDIR <= '1' when cpua(7 downto 1)="0001111" and iow='0' else '0';
  sndBC1  <= cpua(0);
	  
 -- Delta-Sigma DAC for audio (one channel, mono in this implementation)
 audiodac : entity work.dac
    port map ( 
      clk_i   => clk42m,
      res_n_i => swres and pllLocked,
      dac_i   => audiomix(8 downto 1), --oaudio,
      dac_o   => dacout
    ); 

  with tapebits select speaker <=
   "00100000" when "001",
	"00010000" when "000"|"011",
   "00000000" when others;
 
  audiomix <= ('0' & oaudio) + ('0' & speaker); 
 	 
  AUDIO_L <= dacout;
  AUDIO_R <= dacout; 	

	-- fix palette for now
	--with rgbi select rgb <=
	with rgbi select ht_rgb <=
	 "000000000000000000" when "0000",
	 "000000000000100000" when "0001",
	 "000000100000000000" when "0010",
	 "000000100000100000" when "0011",
	 "100000000000000000" when "0100",
	 "100000000000100000" when "0101",
	 "110000011000000000" when "0110",
	 "100000100000100000" when "0111",
	 "110000110000110000" when "1000",
	 "000000000000111100" when "1001",
	 "000000111100000000" when "1010",
	 "000000111100111100" when "1011",
	 "111110000000000000" when "1100",
	 "111100000000111100" when "1101",
	 "111110111110000000" when "1110",
	 "111110111110111110" when others;

	 
--  user_io: work.mist.user_io
--   generic map (STRLEN => CONF_STR'length)
--   port map (
--		clk_sys   => clk42m,
--	    conf_str => to_slv(CONF_STR),
--	
--		SPI_CLK   => SPI_SCK    ,
--      SPI_SS_IO => CONF_DATA0 ,
--      SPI_MISO  => SPI_DO     ,
--      SPI_MOSI  => SPI_DI     ,
--
--		status    => status,
--		buttons   => buttons,
--		 
--		-- ps2 interface
--		ps2_kbd_clk    => ps2CLK,
--		ps2_kbd_data   => ps2DAT,
--		ps2_mouse_clk  => mps2CLK,
--		ps2_mouse_data => mps2DAT,
--		 
--		joystick_0 => joy0,
--      joystick_1 => joy1,
--		
--		scandoubler_disable => scandoubler_disable,
--		ypbpr => ypbpr
--	); 


mist_video : work.mist.mist_video
	generic map (
		SD_HCNT_WIDTH => 10,
		OSD_COLOR => "001")
	port map (
		clk_sys => clk42m,
		scandoubler_disable => scandoubler_disable,
		scanlines => status(2 downto 1),
		ypbpr   => ypbpr,
		rotate  => "00",
      SPI_SCK => SPI_SCK,
      SPI_SS3 => SPI_SS2,
       SPI_DI => SPI_DI,

            R => ht_rgb(5 downto 0),
            G => ht_rgb(11 downto 6),
            B => ht_rgb(17 downto 12),
        HSync => hs,
        VSync => vs,

        VGA_R(5 downto 1) => VGA_R,
        VGA_G(5 downto 1) => VGA_G,
        VGA_B(5 downto 1) => VGA_B,
		 VGA_HS => VGA_HS,
		 VGA_VS => VGA_VS,
		 osd_enable => osd_enable
);
  
  sdram_inst : sdram
    port map( sd_data => SDRAM_DQ,
              sd_addr => SDRAM_A,
               sd_dqm => sdram_dqm,
                sd_cs => SDRAM_nCS,
                sd_ba => SDRAM_BA,
                sd_we => SDRAM_nWE,
               sd_ras => SDRAM_nRAS,
               sd_cas => SDRAM_nCAS,
                  clk => clk42m,
               clkref => clkref,
                 init => not pllLocked,
                  din => ram_din,
                 addr => ram_addr,
                   we => ram_we,
                   oe => ram_oe,
                 dout => ram_dout
    ); 	
  --ram_addr <= "000000000" & cpua when dn_go='0' else dn_addr_r; 
  ram_din <= cpudo when dn_go='0' else dn_data_r;
  ram_we <= ((not memw) and (cpua(15) or cpua(14))) when dn_go='0' else dn_wr_r; 
  --ram_oe <= not memr when dn_go='0' else '0';
  
  ram_addr <= "0" & io_ram_addr when iorrd='1' else "000000000" & cpua when dn_go='0' else dn_addr_r; 
  ram_oe <= '1' when iorrd='1' else not memr when dn_go='0' else '0';
	 
  -- sdram interface
  SDRAM_CKE <= '1';
  SDRAM_DQMH <= sdram_dqm(1);
  SDRAM_DQML <= sdram_dqm(0); 	 

  
  dataio : data_io
    generic map (STRLEN => CONF_STR'length)
    port map (
	   sck 	=> 	SPI_SCK,
		ss    =>  	SPI_SS2,
		sdi	=>		SPI_DI,
		sdo	=>		SPI_DO,
		
		data_in	=> osd_s and keys_s,
		conf_str	=> to_slv(CONF_STR),
		status	=> status,
		
		downloading => dn_go,
		--size        => ioctl_size,
		index       => dn_idx,

		-- ram interface
		clk 	=> 	clk42m,
		wr    =>    dn_wr,
		addr  =>		dn_addr,
		data  =>		dn_data	 
	 );

  process(clk42m)
  -- sync wr from data io to sdram clkref
  begin
    if rising_edge(clk42m) then
	   if dn_wr='1' then
		  dn_wr_new <= '1';
		  dn_data_r <= dn_data;
		  dn_addr_r <= dn_addr;
		end if;
		if clkref = '1' then
			if dn_wr_new = '1' then
				dn_wr_r <= '1';
				dn_wr_new <= '0';
			else
				dn_wr_r <= '0';
			end if;
		end if;
	 end if;
  end process; 
  
 process (clk42m)
 begin
   if rising_edge(clk42m) then  
		if pllLocked='0' or status(6)='1' or btn_n_i(4) = '0' then
		  res_cnt <= "000000";
		else
		  if (res_cnt/="111111") then
			 res_cnt <= res_cnt+1;
		  end if;
		end if;
   end if;
 end process;
  
 cpuClkEn <= '1' when clk42div = 0 else '0'; -- 42/24 = 1.75 MHz
 clkref <= '1' when clk42div = 0 or clk42div = 12 else '0'; -- 42/24 = 1.75 MHz
 autores <= '1' when res_cnt="111111" else '0';   


 process (clk42m, dn_go,autores)
 begin
   if dn_go='1' or autores='0' then
	  io_ram_addr <= x"010000"; -- above 64k
	  iorrd_r<='0';
   elsif rising_edge(clk42m) then  
		if cpuClkEn = '1' then
		  if iow='0' and cpua(7 downto 0)=x"ff" then 
		    tapebits <= cpudo(2 downto 0);
		  end if;
		  if iow='0' and cpua(7 downto 2)="000001" then -- out 4 5 6
			 case cpua(1 downto 0) is
				when "00"=> io_ram_addr(7 downto 0) <= cpudo;
				when "01"=> io_ram_addr(15 downto 8) <= cpudo;
				when "10"=> io_ram_addr(23 downto 16) <= cpudo;
				when others => null;
			 end case;
		  end if;
		  iorrd_r<=iorrd;
		  if iorrd='0' and iorrd_r='1' then
			 io_ram_addr <= io_ram_addr + 1;
		  end if;
		end if;
	end if;
 end process;

 		--start the microcontroller OSD menu after the power on
		process (clk42m, autores, osd_s)
		begin
			if rising_edge(clk42m) then
				if pllLocked = '0' then
					power_on_s <= (others=>'1');
				elsif power_on_s /= x"0000" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '1';
					osd_s <= "00111111";
				else
					power_on_reset <= '0';
					
				end if;
				
				if dn_go = '1' and osd_s <= "00111111" then
					osd_s <= "11111111";
				end if;
				
			end if;
		end process;

 
end Behavioral;

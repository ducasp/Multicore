/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module jt_gng(
	input [1:0] UP,
	input [1:0] DOWN,
	input [1:0] LEFT,
	input [1:0] RIGHT,
	input [1:0] SHOT2,
	input [1:0] SHOT1,
	output [1:0] COUNTER,
	output SYNC,
	inout [7:0] DIPSW_B,
	inout [7:0] DIPSW_A,
	input [1:0] COIN,
	input [1:0] START,
	input [1:0] RESERVED
);

wire [7:0]	DB, OBJ;
wire [12:0] AB;
wire [2:0]	SCD;
wire SCRWIN, OH;
wire SCRPO_b;
wire SCRCS_b;
wire MRDY2_b;
wire BLCNTEN_b;
wire WRB_b;
wire RDB_b;
wire AKB_b;
wire RQB_b;
wire ALC2_b;
wire OKOUT_b;
wire G4H;

jt_gng_a boardA (
	.UP       (UP       ),
	.DOWN     (DOWN     ),
	.LEFT     (LEFT     ),
	.RIGHT    (RIGHT    ),
	.SHOT2    (SHOT2    ),
	.SHOT1    (SHOT1    ),
	.COUNTER  (COUNTER  ),
	.SYNC     (SYNC     ),
	.DIPSW_B  (DIPSW_B  ),
	.DIPSW_A  (DIPSW_A  ),
	.COIN     (COIN     ),
	.START    (START    ),
	.RESERVED (RESERVED ),
	// Board-to-board connector
	.RDB_b    (RDB_b    ),
	.WRB_b    (WRB_b    ),
	.AB       (AB       ),
	.DB       (DB       ),
	.BLCNTEN_b(BLCNTEN_b),
	.RQB_b    (RQB_b    ),
	.ALC2_b   (ALC2_b   ),
	.AKB_b    (AKB_b    ),
	.OKOUT_b  (OKOUT_b  ),
	.V1       (V1       ),
	.V2       (V2       ),
	.V4       (V4       ),
	.V8       (V8       ),
	.V16      (V16      ),
	.V32      (V32      ),
	.V64      (V64      ),
	.V128     (V128     ),
	.FLIP     (FLIP     ),
	.OH		  (OH		),
	.CBCS_b   (CBCS_b   ),
	.SCRCS_b  (SCRCS_b  ),
	.MRDY2_b  (MRDY2_b  ),
	.G6M      (G6M      ),
	.G4H	  (G4H		),
	.G4_3H	  (G4_3H    ),
	.HINIT_b  (HINIT_b  ),
	.H256     (H256     ),
	.H128     (H128     ),
	.H64      (H64      ),
	.H32      (H32      ),
	.H16      (H16      ),
	.H8       (H8       ),
	.H4       (H4       ),
	.H2       (H2       ),
	.H1       (H1       ),
	.LHBL	  (LHBL		),
	.OBJ      (OBJ      ),
	.SCRWIN   (SCRWIN   ),
	.SCD      (SCD      ),
	.SCRX     (SCRX     ),
	.SCRY     (SCRY     ),
	.SCRZ     (SCRZ     )
);

jt_gng_b boardB (
	.RDB_b    (RDB_b    ),
	.WRB_b    (WRB_b    ),
	.AB       (AB       ),
	.DB       (DB       ),
	.BLCNTEN_b(BLCNTEN_b),
	.RQB_b    (RQB_b    ),
	.ALC2_b   (ALC2_b   ),
	.AKB_b    (AKB_b    ),
	.OKOUT_b  (OKOUT_b  ),
	.V1       (V1       ),
	.V2       (V2       ),
	.V4       (V4       ),
	.V8       (V8       ),
	.V16      (V16      ),
	.V32      (V32      ),
	.V64      (V64      ),
	.V128     (V128     ),
	.FLIP     (FLIP     ),
	.OH		  (OH		),
	.CBCS_b   (CBCS_b   ),
	.SCRCS_b  (SCRCS_b  ),
	.MRDY2_b  (MRDY2_b  ),
	.G6M      (G6M      ),
	.G4H	  (G4H		),
	.G4_3H	  (G4_3H    ),
	.HINIT_b  (HINIT_b  ),
	.H256     (H256     ),
	.H128     (H128     ),
	.H64      (H64      ),
	.H32      (H32      ),
	.H16      (H16      ),
	.H8       (H8       ),
	.H4       (H4       ),
	.H2       (H2       ),
	.H1       (H1       ),
	.LHBL	  (LHBL		),
	.OBJ      (OBJ      ),
	.SCRWIN   (SCRWIN   ),
	.SCD      (SCD      ),
	.SCRX     (SCRX     ),
	.SCRY     (SCRY     ),
	.SCRZ     (SCRZ     )
);


endmodule


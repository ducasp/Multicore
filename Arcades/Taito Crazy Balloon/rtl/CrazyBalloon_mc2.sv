/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module CrazyBalloon_mc2 (
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2
);

`include "rtl\build_id.v" 

localparam CONF_STR = {
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "O6,Ram Test,On,Off;",
    "OOR,CRT H adjust,0,+1,+2,+3,+4,+5,+6,+7,-8,-7,-6,-5,-4,-3,-2,-1;",
    "OSV,CRT V adjust,0,+1,+2,+3,+4,+5,+6,+7,-8,-7,-6,-5,-4,-3,-2,-1;",
    "T0,Reset;",
    "V,v1.21.",`BUILD_DATE
};

//      <dip bits="1"     name="Cabinet" ids="Cocktail,Upright"/>
//      <dip bits="2,3"   name="Lives" ids="2,3,4,5"/>
//      <dip bits="4"     name="Bonus life" ids="5000,10000"/>
//      <dip bits="5,6,7" name="Coinage" ids="4c/1cr,3c/1cr,2c/1cr,1c/1cr,1c/2cr,1c/3cr,1c/4cr,Disable"/>

wire       rotate = status[2];
wire [1:0] scanlines = status[4:3];
wire       blend = status[5];

assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;
assign stm_rst_o        = 1'bz;

assign AUDIO_R = AUDIO_L;

wire clk_vid, clk_sys;
reg  [1:0] clk_div = 2'd0;
wire cpu_clk = clk_div[1];      // 2.49675 Mhz
wire pix_clk = clk_div[0];      // 4.9935  Mhz
pll pll(
    .inclk0(clock_50_i),
    .c0(clk_vid),// 39.948 Mhz
    .c1(clk_sys)//   9.987 Mhz
    );
    
// Divider for other clocks (7474 and 74161 on PCB)
always @(posedge clk_sys) begin
    clk_div <= clk_div + 1;
end

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        key_pressed;
wire  [7:0] key_code;
wire        key_strobe;
wire [15:0] audio;
wire hs, vs;
wire hb, vb;
wire blankn = ~(hb | vb);
wire [1:0] r, g, b;
wire [ 3:0] hoffset, voffset;
assign { voffset, hoffset } = status[31:24];
wire [7:0] sw = {3'b111,1'b0,1'b0,1'b0,1'b1,status[6]};

CRAZYBALLOON CRAZYBALLOON
(
    .O_VIDEO_R(r),
    .O_VIDEO_G(g),
    .O_VIDEO_B(b),
    .O_HSYNC(hs),
    .O_VSYNC(vs),
    .O_HBLANK(hb),
    .O_VBLANK(vb),
    .I_H_OFFSET(hoffset),
    .I_V_OFFSET(voffset),
    .O_AUDIO(audio),
    .dipsw1(sw),
    .in0({~m_right2,~m_left2,~m_down2,~m_up2,~m_right,~m_left,~m_down,~m_up}),
    .in1({1'b0,btn_coin,~btn_two_players,~btn_one_player,1'b1,1'b1,1'b1,1'b1}),
    .dn_addr(),
    .dn_data(),
    .dn_wr(),
    .dn_ld(),
    .RESET(status[0] | ~btn_n_i[4]),
    .PIX_CLK(pix_clk),
    .CPU_CLK(cpu_clk),
    .CLK(clk_sys)
);

wire [5:0] vga_r_s; 
wire [5:0] vga_g_s; 
wire [5:0] vga_b_s; 

mist_video #(.COLOR_DEPTH(2), .SD_HCNT_WIDTH(10)) mist_video
(
    .clk_sys(clk_vid),
    .SPI_SCK(SPI_SCK),
    .SPI_SS3(SPI_SS2),
    .SPI_DI(SPI_DI),

    .R(blankn ? r : 0),
    .G(blankn ? g : 0),
    .B(blankn ? b : 0),
    .HSync(~hs),
    .VSync(vs),

    .VGA_R          ( vga_r_s          ),
    .VGA_G          ( vga_g_s          ),
    .VGA_B          ( vga_b_s          ),
    .VGA_VS(VGA_VS),
    .VGA_HS(VGA_HS),

    .ce_divider(1'b0),
    .rotate({2'b01}),
    .blend(blend),
    .scanlines(scanlines),
    .scandoubler_disable(scandoublerD),
    .osd_enable      ( osd_enable )
    
    );

assign VGA_R = vga_r_s[5:1];
assign VGA_G = vga_g_s[5:1];
assign VGA_B = vga_b_s[5:1];

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in        ( keys_s ),
    .conf_str       ( CONF_STR      ),
    .status         ( status        ) 
);

dac #(
    .C_bits(16))
dac(
    .clk_i(clk_vid),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );


//-----------------------
reg [3:0] clk_kbd;

always @(posedge clk_sys)
begin
    clk_kbd <= clk_kbd + 1'd1;
end

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_kbd[1] ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 )) k_joystick
(
  .clk          ( clk_kbd[1] ),
  .kbdint       ( kbd_intr ),
  .kbdscancode  ( kbd_scancode ), 
  
    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
          
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),
        
    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),
        
    //-- fire12-1, up, down, left, right

    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
        
    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable     ( osd_enable ),
    
    //-- sega joystick
    .sega_clk       ( hs ),
    .sega_strobe    ( joyX_p7_o )
        
        
);

endmodule

--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-----------------------------------------------------------------------
-- FPGA MOONCRESTA CLOCK GEN
--
-- Version : 1.00
--
-- Copyright(c) 2004 Katsumi Degawa , All rights reserved
--
-- Important !
--
-- This program is freeware for non-commercial use.
-- The author does not guarantee this program.
-- You can use this at your own risk.
--
-----------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

entity CLOCKGEN is
port (
	CLKIN_IN        : in  std_logic;
	RST_IN          : in  std_logic;
	--
	O_CLK_24M       : out std_logic;
	O_CLK_18M       : out std_logic;
	O_CLK_12M       : out std_logic;
	O_CLK_06M       : out std_logic
);
end;

architecture RTL of CLOCKGEN is
	signal state                    : std_logic_vector(1 downto 0) := (others => '0');
	signal ctr1                     : std_logic_vector(1 downto 0) := (others => '0');
	signal ctr2                     : std_logic_vector(2 downto 0) := (others => '0');
	signal CLKFB_IN                 : std_logic := '0';
	signal CLK0_BUF                 : std_logic := '0';
	signal CLKFX_BUF                : std_logic := '0';
	signal CLK_72M                  : std_logic := '0';
	signal I_DCM_LOCKED             : std_logic := '0';

begin
	dcm_inst : DCM_SP
	generic map (
		CLKFX_MULTIPLY => 9,
		CLKFX_DIVIDE   => 4,
		CLKIN_PERIOD   => 31.25
	)
	port map (
		CLKIN    => CLKIN_IN,
		CLKFB    => CLKFB_IN,
		RST      => RST_IN,
		CLK0     => CLK0_BUF,
		CLKFX    => CLKFX_BUF,
		LOCKED   => I_DCM_LOCKED
	);

	BUFG0  : BUFG  port map (I=> CLK0_BUF,  O => CLKFB_IN);
	BUFG1  : BUFG  port map (I=> CLKFX_BUF, O => CLK_72M);
	O_CLK_06M <= ctr2(2);
	O_CLK_12M <= ctr2(1);
	O_CLK_24M <= ctr2(0);
	O_CLK_18M <= ctr1(1);

	-- generate all clocks, 36Mhz, 18Mhz, 24Mhz, 12Mhz and 6Mhz
	process(CLK_72M)
	begin
		if rising_edge(CLK_72M) then
			if (I_DCM_LOCKED = '0') then
				state <= "00";
				ctr1 <= (others=>'0');
				ctr2 <= (others=>'0');
			else
				ctr1 <= ctr1 + 1;
				case state is
					when "00" => state <= "01"; ctr2 <= ctr2 + 1;
					when "01" => state <= "10"; ctr2 <= ctr2 + 1;
					when "10" => state <= "00";
					when "11" => state <= "00";
					when others => null;
				end case;
			end if;
		end if;
	end process;
end RTL;

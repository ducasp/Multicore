--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;

package mc_pack is
    constant HW_GALAXIAN : integer := 0;
    constant HW_MOONCR   : integer := 1;
    constant HW_AZURIAN  : integer := 2;
    constant HW_BLKHOLE  : integer := 3;
    constant HW_CATACOMB : integer := 4;
    constant HW_CHEWINGG : integer := 5;
    constant HW_DEVILFSH : integer := 6;
    constant HW_KINGBAL  : integer := 7;
    constant HW_MRDONIGH : integer := 8;
    constant HW_OMEGA    : integer := 9;
    constant HW_ORBITRON : integer := 10;
    constant HW_PISCES   : integer := 11;
    constant HW_UNIWARS  : integer := 12;
    constant HW_VICTORY  : integer := 13;
    constant HW_WAROFBUG : integer := 14;
    constant HW_ZIGZAG   : integer := 15; -- doesn't work yet
    constant HW_TRIPLEDR : integer := 16;
end;
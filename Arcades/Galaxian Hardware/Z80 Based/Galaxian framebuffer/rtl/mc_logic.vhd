--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
-- FPGA MOONCRESTA    LOGIC IP MODULE
--
-- Version : 1.00
--
-- Copyright(c) 2004 Katsumi Degawa , All rights reserved
--
-- Important !
--
-- This program is freeware for non-commercial use.
-- The author does not guarantee this program.
-- You can use this at your own risk.
--
-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- 74xx138
-- 3-to-8 line decoder
-------------------------------------------------------------------------------
entity LOGIC_74XX138 is
	port (
		I_G1  : in  std_logic;
		I_G2a : in  std_logic;
		I_G2b : in  std_logic;
		I_Sel : in  std_logic_vector(2 downto 0);
		O_Q   : out std_logic_vector(7 downto 0)
	);
end logic_74xx138;

architecture RTL of LOGIC_74XX138 is
	signal I_G : std_logic_vector(2 downto 0) := (others => '0');

begin
	I_G <= I_G1 & I_G2a & I_G2b;

	xx138 : process(I_G, I_Sel)
	begin
		if(I_G = "100" ) then
			case I_Sel is
				when "000" => O_Q <= "11111110";
				when "001" => O_Q <= "11111101";
				when "010" => O_Q <= "11111011";
				when "011" => O_Q <= "11110111";
				when "100" => O_Q <= "11101111";
				when "101" => O_Q <= "11011111";
				when "110" => O_Q <= "10111111";
				when "111" => O_Q <= "01111111";
				when others => null;
			end case;
		 else
				O_Q <= (others => '1');
		 end if;
	end process;
end RTL;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- 74xx139
-- 2-to-4 line decoder
-------------------------------------------------------------------------------
entity LOGIC_74XX139 is
	port (
		I_G   : in  std_logic;
		I_Sel : in  std_logic_vector(1 downto 0);
		O_Q   : out std_logic_vector(3 downto 0)
	);
end;

architecture RTL of LOGIC_74XX139 is
begin
	xx139 : process (I_G, I_Sel)
	begin
		if I_G = '0' then
			case I_Sel is
				when "00" => O_Q <= "1110";
				when "01" => O_Q <= "1101";
				when "10" => O_Q <= "1011";
				when "11" => O_Q <= "0111";
				when others => null;
			end case;
		else
			O_Q <= "1111";
		end if;
	end process;
end RTL;


derive_pll_clocks -create_base_clocks

derive_clock_uncertainty

#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {SPI_SCK}  -period 27.777  [get_ports {SPI_SCK}]

set sdram_clk "pll|altpll_component|auto_generated|pll1|clk[0]"
set vid_clk   "pll|altpll_component|auto_generated|pll1|clk[2]"
set game_clk  "pll|altpll_component|auto_generated|pll1|clk[1]"
set aud_clk   "pll|altpll_component|auto_generated|pll1|clk[3]"

#**************************************************************
# Create Generated Clock
#**************************************************************


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay  -clock_fall -clock [get_clocks {clock_50_i}]  1.000 [get_ports {clock_50_i}]
set_input_delay -add_delay  -clock_fall -clock [get_clocks {SPI_SCK}]  1.000 [get_ports {SPI_DI}]
set_input_delay -add_delay  -clock_fall -clock [get_clocks {SPI_SCK}]  1.000 [get_ports {SPI_SCK}]
set_input_delay -add_delay  -clock_fall -clock [get_clocks {SPI_SCK}]  1.000 [get_ports {SPI_SS2}]

set_input_delay -clock [get_clocks $sdram_clk] -reference_pin [get_ports {SDRAM_CLK}] -max 6.4 [get_ports SDRAM_DQ[*]]
set_input_delay -clock [get_clocks $sdram_clk] -reference_pin [get_ports {SDRAM_CLK}] -min 3.2 [get_ports SDRAM_DQ[*]]

#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay   -clock [get_clocks {SPI_SCK}] 1.000 [get_ports {SPI_DO}]
set_output_delay -add_delay   -clock [get_clocks $aud_clk]  1.000 [get_ports {AUDIO_L}]
set_output_delay -add_delay   -clock [get_clocks $aud_clk]  1.000 [get_ports {AUDIO_R}]
set_output_delay -add_delay   -clock [get_clocks $game_clk] 1.000 [get_ports {LED}]
set_output_delay -add_delay   -clock [get_clocks $vid_clk]  1.000 [get_ports {VGA_*}]

set_output_delay -clock [get_clocks $sdram_clk] -reference_pin [get_ports {SDRAM_CLK}] -max 1.5 [get_ports {SDRAM_D* SDRAM_A* SDRAM_BA* SDRAM_n* SDRAM_CKE}]
set_output_delay -clock [get_clocks $sdram_clk] -reference_pin [get_ports {SDRAM_CLK}] -min -0.8 [get_ports {SDRAM_D* SDRAM_A* SDRAM_BA* SDRAM_n* SDRAM_CKE}]

#**************************************************************
# Set Clock Groups
#**************************************************************

set_clock_groups -asynchronous -group [get_clocks {SPI_SCK}] -group [get_clocks {pll|altpll_component|auto_generated|pll1|clk[*]}]
set_clock_groups -asynchronous -group [get_clocks $sdram_clk] -group [get_clocks $aud_clk]
set_clock_groups -asynchronous -group [get_clocks $game_clk] -group [get_clocks $aud_clk]

#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************

set_multicycle_path -to {VGA_*[*]} -setup 3
set_multicycle_path -to {VGA_*[*]} -hold 2



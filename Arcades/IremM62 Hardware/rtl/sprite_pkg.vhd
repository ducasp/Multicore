--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.video_controller_pkg.all;
use work.platform_variant_pkg.all;

package sprite_pkg is

  subtype SPRITE_N_t is std_logic_vector(11 downto 0);
  subtype SPRITE_A_t is std_logic_vector(8 downto 0);
  subtype SPRITE_D_t is std_logic_vector(7 downto 0);
  
  type from_SPRITE_REG_t is record
    n         : SPRITE_N_t;
    x         : std_logic_vector(10 downto 0);
    y         : std_logic_vector(10 downto 0);
    xflip     : std_logic;
    yflip     : std_logic;
    colour    : std_logic_vector(7 downto 0);
    pri       : std_logic;
  end record;
  
  type to_SPRITE_REG_t is record
    clk       : std_logic;
    clk_ena   : std_logic;
    wr        : std_logic;
    a         : SPRITE_A_t;
    d         : SPRITE_D_t;
  end record;

  function NULL_TO_SPRITE_REG return to_SPRITE_REG_t;

  subtype SPRITE_ROW_D_t is std_logic_vector(23 downto 0);
  subtype SPRITE_ROW_A_t is std_logic_vector(15 downto 0);

  type to_SPRITE_CTL_t is record
    ld        : std_logic;
    d         : SPRITE_ROW_D_t;
    height    : integer range 0 to 3;
  end record;

  type from_SPRITE_CTL_t is record
    a         : SPRITE_ROW_A_t;
    set       : std_logic;
    pal_a     : std_logic_vector(7 downto 0);
  end record;

  function NULL_TO_SPRITE_CTL return to_SPRITE_CTL_t;

  component sprite_array is
    generic
    (
      N_SPRITES   : integer;
      DELAY       : integer
    );
    port
    (
      reset       : in std_logic;
      hwsel       : HWSEL_t;
      hires       : in std_logic;
      sprite_prom : in prom_a(0 to 31);

      -- register interface
      reg_i       : in to_SPRITE_REG_t;

      -- video control signals
      video_ctl   : in from_VIDEO_CTL_t;

      -- extra data
      graphics_i  : in to_GRAPHICS_t;

      -- sprite data
      row_a       : out SPRITE_ROW_A_t;
      row_d       : in SPRITE_ROW_D_t;

      -- video data
      pal_a       : out std_logic_vector(7 downto 0);
      set         : out std_logic;
      spr0_set    : out std_logic
    );
  end component sprite_array;

  function flip_row
  (
    row_in      : std_logic_vector;
    flip        : std_logic
  )
  return SPRITE_ROW_D_t;

  function flip_1
  (
    d_i         : std_logic_vector;
    flip        : std_logic
  )
  return std_logic_vector;

end package sprite_pkg;

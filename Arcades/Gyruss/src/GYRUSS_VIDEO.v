/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// Copyright (c) 2020 MiSTer-X

module GYRUSS_VIDEO
(
	input				MCLK,

	input	  [8:0]	PH,
	input	  [8:0]	PV,
	input				VFLP,

	output			BGCL,
	output  [9:0]	BGVA,
	input  [15:0]	BGVD,

	output			SPCL,
	output  [7:0]	SPAA,
	input   [7:0]	SPAD,

	output 			PCLK,
	output  [7:0]	POUT,	

	input				ROMCL,
	input  [16:0]	ROMAD,
	input	  [7:0]	ROMID,
	input	 			ROMEN
);
	
// Clocks
reg [2:0] clkdiv;
always @(posedge MCLK) clkdiv <= clkdiv+1;
wire VCLKx8 = MCLK;
wire VCLKx4 = clkdiv[0];
wire VCLKx2 = clkdiv[1];
wire VCLK   = clkdiv[2];

// BG Scanline Generator
assign		BGCL = VCLKx4;
wire		   BGPR;
wire  [4:0] BGPN;
GYRUSS_BG bg(VCLKx2,PH,PV,VFLP,BGVA,BGVD,BGPR,BGPN, ROMCL,ROMAD,ROMID,ROMEN);

// Sprite Scanline Generator
wire 		  SPOQ;
wire [4:0] SPPN;
GYRUSS_SPRITE spr(VCLKx8,VCLK,PH,PV,SPCL,SPAA,SPAD,SPOQ,SPPN, ROMCL,ROMAD,ROMID,ROMEN);

// Color Mixer & Palette
wire [4:0] PALN = BGPR ? BGPN :
						SPOQ ? SPPN :
								 BGPN ;

DLROM #(8,8) pal(VCLK,PALN,POUT, ROMCL,ROMAD,ROMID,ROMEN && ROMAD[16:8]=={4'hB,5'h12});
assign PCLK = ~VCLK;

endmodule


module GYRUSS_BG
(
	input				VCLKx2,
	
	input  [8:0]	PH,
	input	 [8:0]	PV,
	input				VFLP,

	output [9:0]	BGVA,
	input  [15:0]	BGVD,
	
	output reg		BGPR,
	output [4:0]	BGPN,

	input				ROMCL,
	input  [16:0]	ROMAD,
	input	 [7:0]	ROMID,
	input	 			ROMEN
);

wire  [8:0] BGHP = PH;
wire  [8:0] BGVP = PV;

assign 		BGVA = {BGVP[7:3]^{5{VFLP}},BGHP[7:3]^{5{VFLP}}};

wire  [8:0] BGNO = {BGVD[13],BGVD[7:0]};
wire  [3:0] BGPT = BGVD[11:8];
wire        BGFH = BGVD[14]^VFLP;
wire        BGFV = BGVD[15]^VFLP;
wire [12:0] BGCA = {BGNO,(BGHP[2]^BGFH),(BGVP[2:0]^{3{BGFV}})};
wire  [7:0] BGCD;

wire  [7:0] BGPS = BGCD << (BGHP[1:0]^{2{BGFH}});
wire  [1:0] BGPX = {BGPS[3],BGPS[7]};
wire  [5:0] BGCN = {BGPT,BGPX};
wire  [3:0] BGCT;

always @(negedge VCLKx2) BGPR <= BGVD[12];
assign BGPN = {1'b1,BGCT};

DLROM #(13,8) bgch( VCLKx2,BGCA,BGCD, ROMCL,ROMAD,ROMID,ROMEN && ROMAD[16:13]=={4'h4});
DLROM #( 8,4) cltb(~VCLKx2,BGCN,BGCT, ROMCL,ROMAD,ROMID,ROMEN && ROMAD[16: 8]=={4'hB,5'h11});

endmodule

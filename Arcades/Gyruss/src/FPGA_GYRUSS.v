/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// FPGA Gyruss
// Copyright (c) 2020 MiSTer-X

module FPGA_GYRUSS
(
	input				MCLK,
	input				SCLK,
	input				RESET,

	input  [7:0]	INP0,
	input	 [7:0]	INP1,
	input	 [7:0]	INP2,

	input	 [7:0]	DSW0,
	input	 [7:0]	DSW1,
	input	 [7:0]	DSW2,

	input  [8:0]	PH,
	input	 [8:0]	PV,
	output			PCLK,
	output [7:0]	POUT,
	
	output [15:0]	SND_L,
	output [15:0]	SND_R,

	input				ROMCL,
	input  [16:0]	ROMAD,
	input	 [7:0]	ROMDT,
	input	 			ROMEN
);

wire			SPCL;
wire  [7:0]	SPAA;
wire  [7:0]	SPAD;

wire			BGCL;
wire  [9:0] BGVA;
wire [15:0] BGVD;

wire			SHCL,SHMW;
wire [10:0]	SHMA;
wire  [7:0]	SHMD,SHWD;

wire		   FLPV;

wire        SNDRQ;
wire  [7:0] SNDNO;


GYRUSS_MAIN main(
	MCLK,RESET,
	PH,PV,
	INP0,INP1,INP2,DSW0,DSW1,DSW2,
	BGCL,BGVA,BGVD,
	SHCL,SHMA,SHMD,SHMW,SHWD,
	FLPV,
	SNDRQ,SNDNO,

	ROMCL,ROMAD,ROMDT,ROMEN
);


GYRUSS_SUB sub(
	MCLK,RESET,PH,PV,
	SHCL,SHMA,SHMD,SHMW,SHWD,
	SPCL,SPAA,SPAD,
	
	ROMCL,ROMAD,ROMDT,ROMEN
);


GYRUSS_VIDEO video(
	MCLK,
	PH,PV,FLPV,
	BGCL,BGVA,BGVD,
	SPCL,SPAA,SPAD,
	PCLK,POUT,

	ROMCL,ROMAD,ROMDT,ROMEN
);


GYRUSS_SOUND sound(
	MCLK,SCLK,RESET,SNDRQ,SNDNO,
	SND_L,SND_R,

	ROMCL,ROMAD,ROMDT,ROMEN
);

endmodule


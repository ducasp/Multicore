//============================================================================
//  Arcade: Donkey Kong
//
//  Port to MiSTer
//  Copyright (C) 2017 Sorgelig
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module dkong_mc2p
(
 // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on

);

localparam CONF_STR = {
    "P,Donkey Kong.dat;", 
    "S,DAT,Alternative ROM...;",
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blending,Off,On;",
    "O7,Scandoubler,On,Off;",
    "O89,Lives,3,4,5,6;",
    "OAB,Bonus,7000,10000,15000,20000;",
    "OC,Cabinet,Upright,Cocktail;",
    "T6,Reset;",
    "V,v"
};

//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;
assign LED  = ~ioctl_download;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//no SDRAM for this core
assign SDRAM_nCS = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] osd_s = 8'b11111111;
PumpSignal PumpSignal (clk_49, ~pll_locked, ioctl_download, osd_s);

//-----------------------------------------------------------------

////////////////////   CLOCKS   ///////////////////

wire clk_sys,clk_49,pll_locked;

pll pll
(
    .inclk0(clock_50_i),
    .c0(clk_49),
    .c1(clk_sys),
    .locked(pll_locked)
);

wire clk_25m2,clk_40;    
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

///////////////////////////////////////////////////

wire [31:0] status;
wire  [1:0] buttons;
wire        forced_scandoubler;

wire        ioctl_download;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

wire [10:0] ps2_key;

wire [15:0] joystick_0, joystick_1;
wire [15:0] joy = joystick_0 | joystick_1;

wire [21:0] gamma_bus;

/*
hps_io #(.STRLEN($size(CONF_STR)>>3)) hps_io
(
    .clk_sys(clk_sys),
    .HPS_BUS(HPS_BUS),

    .conf_str(CONF_STR),

    .buttons(buttons),
    .status(status),
    .status_menumask(direct_video),
    .forced_scandoubler(forced_scandoubler),
    .gamma_bus(gamma_bus),
    .direct_video(direct_video),

    .ioctl_download(ioctl_download),
    .ioctl_wr(ioctl_wr),
    .ioctl_addr(ioctl_addr),
    .ioctl_dout(ioctl_dout),

    .joystick_0(joystick_0),
    .joystick_1(joystick_1),
    .ps2_key(ps2_key)
);*/

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( osd_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    
    .ioctl_download( ioctl_download  ),
    .ioctl_index   (   ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);


// https://www.arcade-museum.com/dipswitch-settings/7610.html
//wire [7:0]W_DIP={1'b1,1'b0,1'b0,1'b0,`DIP_BOUNS,`DIP_LIVES};
// 1 bit cocktail  - 3 bits - coins - 2 bits bonus - 2 bits lives 
wire [7:0]m_dip = { ~status[12] , 1'b0,1'b0,1'b0 , status[11:10], status[9:8]};

wire vblank;
wire hs, vs;
wire [2:0] r,g;
wire [1:0] b;

reg ce_pix;
always @(posedge clk_49) begin
        reg [2:0] div;

        div <= div + 1'd1;
        ce_pix <= !div;
end

//=================================
wire clk_pix, hbl0;
wire hblank = hbl[7];
reg [7:0] hbl;
always @(posedge clk_pix) 
begin
        hbl <= (hbl<<1)|hbl0;
end

wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

framebuffer #(256,224,8) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( clk_pix ),
        .RGB_i      ((~vblank) ? {r,g,b} : 8'b00000000 ),//idx_color_s ),
        .hblank_i   ( hblank ),
        .vblank_i   ( vblank ),
        
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);


//=================================
wire blankn = ~(hblank | vblank);
wire direct_video_s = ~status[7] ^ direct_video;

mist_video #(.COLOR_DEPTH(3),.SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video
(
    .clk_sys( direct_video_s ? clk_sys :(status[1]) ? clk_40 : clk_25m2),

    .SPI_SCK ( SPI_SCK ),
    .SPI_SS3 ( SPI_SS2 ),
    .SPI_DI  ( SPI_DI ),

    .R       ( (direct_video_s) ? (blankn) ? r         : 3'b000 : vga_col_s[7:5]   ),
    .G       ( (direct_video_s) ? (blankn) ? g         : 3'b000 : vga_col_s[4:2]   ),
    .B       ( (direct_video_s) ? (blankn) ? {b,b[0]}  : 3'b000 : {vga_col_s[1:0], vga_col_s[0]} ),
    .HSync   ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync   ( (direct_video_s) ? vs : vga_vs_s ),

    .VGA_R    ( VGA_R  ),
    .VGA_G    ( VGA_G  ),
    .VGA_B    ( VGA_B  ),
    .VGA_HS   ( VGA_HS ),
    .VGA_VS   ( VGA_VS ),

    .rotate(osd_rotate),
    .ce_divider(1'b1),
    .blend(status[5]),
    .scandoubler_disable(direct_video_s),
    .scanlines(status[4:3]),
    .osd_enable(osd_enable)
    );

wire [7:0] audio;

assign AUDIO_R = AUDIO_L;

dac #(
    .C_bits(16))
dac(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i({audio,audio}),
    .dac_o(AUDIO_L)
    );


dkong_top dkong
(
    .I_CLK_24576M(clk_sys),
    .I_RESETn(~( status[6] | ~btn_n_i[4] | ioctl_download)),

    .dn_addr(ioctl_addr[18:0]),
    .dn_data(ioctl_dout),
    .dn_wr(ioctl_wr),

    .O_PIX(clk_pix),

    .I_U1(~m_up),
    .I_D1(~m_down),
    .I_L1(~m_left),
    .I_R1(~m_right),
    .I_J1(~(m_fireA|m_fireB|m_fireC)),
    
    .I_U2(~m_up2),
    .I_D2(~m_down2),
    .I_L2(~m_left2),
    .I_R2(~m_right2),
    .I_J2(~(m_fire2A|m_fire2B|m_fire2C)),

    .I_S1(~btn_one_player),
    .I_S2(~btn_two_players),
    .I_C1(~btn_coin),

    .I_DIP_SW(m_dip),

    
    .O_VGA_R(r),
    .O_VGA_G(g),
    .O_VGA_B(b),
    .O_VGA_H_SYNCn(hs),
    .O_VGA_V_SYNCn(vs),

    .O_H_BLANK(hbl0),
    .O_V_BLANK(vblank),

    .O_SOUND_DAT(audio)
);

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), . CLK_SPEED(24576)) k_joystick
(
    .clk          ( clk_sys ), // 24.576
    .kbdint       ( kbd_intr ),
    .kbdscancode  ( kbd_scancode ), 

    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable     ( osd_enable ),

    //-- sega joystick
    .sega_strobe    ( joy_p7_o )       
);

endmodule

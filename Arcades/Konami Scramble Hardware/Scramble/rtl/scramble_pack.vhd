--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;

package scramble_pack is

    constant I_HWSEL_SCRAMBLE : integer := 0; -- this MUST be set true for scramble, the_end, amidar
    constant I_HWSEL_FROGGER  : integer := 1; -- this MUST be set true for frogger
    constant I_HWSEL_SCOBRA   : integer := 2; -- SuperCobra, TazzMania
    constant I_HWSEL_CALIPSO  : integer := 3; -- Calipso
    constant I_HWSEL_DARKPLNT : integer := 4; -- Dark Planet
    constant I_HWSEL_STRATGYX : integer := 5; -- Strategy X
    constant I_HWSEL_ANTEATER : integer := 6; -- Ant Eater (SCOBRA with obj_ram address line obfuscation)
    constant I_HWSEL_LOSTTOMB : integer := 7; -- Lost Tomb (SCOBRA with obj_ram address line obfuscation)
    constant I_HWSEL_MINEFLD  : integer := 8; -- Minefield (SCOBRA with obj_ram address line obfuscation)
    constant I_HWSEL_RESCUE   : integer := 9; -- Rescue    (SCOBRA with obj_ram address line obfuscation)
    constant I_HWSEL_MARS     : integer := 10; -- Mars
    constant I_HWSEL_TURTLES  : integer := 11; -- Turtles
    constant I_HWSEL_MIMONKEY : integer := 12; -- Mighty Monkey (use mimonscr bootleg ROMs to avoid writing the ROM decryptor)

end;

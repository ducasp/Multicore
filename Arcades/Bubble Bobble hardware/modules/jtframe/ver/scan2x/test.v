/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

module test;

localparam COLORW=4;
localparam DW=COLORW*3;


reg           clk, rst_n;
wire          pxl_cen, pxl2_cen;
reg  [DW-1:0] base_pxl;
wire          HS, VS;
integer       frame_cnt=0;

initial begin
    clk = 0;
    base_pxl = {3{4'b1010}};
    forever #(20.833/2) clk = ~clk; // 48 MHz
end

initial begin
    rst_n = 0;
    #100 rst_n = 1;
end

jtframe_cen48 u_cen(
    .clk    ( clk       ),
    .cen12  ( pxl2_cen  ),
    .cen6   ( pxl_cen   )
);

// Get a random pixel
always @(posedge clk) if(pxl_cen) begin
    //base_pxl <= { base_pxl[DW-2:0], base_pxl[4]^base_pxl[DW-1] };
    base_pxl <= base_pxl+12'h632;
end

always @(posedge VS) begin
    frame_cnt <= frame_cnt + 1;
    if( frame_cnt == 1 ) $finish;
end

jtframe_scan2x #(.HLEN(396)) UUT(
    .rst_n      ( rst_n     ),
    .clk        ( clk       ),
    .pxl_cen    ( pxl_cen   ),
    .pxl2_cen   ( pxl2_cen  ),
    .base_pxl   ( base_pxl  ),
    .HS         ( HS        ),
    .x2_pxl     (           ),
    .x2_HS      (           )
);

jtframe_vtimer u_timer(
    .clk        ( clk       ),
    .pxl_cen    ( pxl_cen   ),
    .HS         ( HS        ),
    .VS         ( VS        )
);
/*
jtgng_timer #(.LAYOUT(5)) u_timer(
    .clk       ( clk      ),
    .cen6      ( pxl_cen  ),
    .HS        ( HS       ),
    .VS        ( VS       )
);
*/

initial begin
    $dumpfile("test.lxt");
    $dumpvars;
end

endmodule

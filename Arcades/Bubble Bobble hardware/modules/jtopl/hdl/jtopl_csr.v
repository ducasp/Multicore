/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* This file is part of JTOPL.

 
    JTOPL program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JTOPL program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JTOPL.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 17-6-2020 

*/

module jtopl_csr #(
    parameter LEN=18, W=32
) ( // Circular Shift Register + input mux
    input           rst,
    input           clk,
    input           cen,
    input   [ 7:0]  din,
    output [W-1:0]  shift_out,

    input           up_mult,
    input           up_ksl_tl,
    input           up_ar_dr,
    input           up_sl_rr,
    input           update_op_I,
    input           update_op_II,
    input           update_op_IV
);


wire [W-1:0] regop_in;

jtopl_sh_rst #(.width(W),.stages(LEN)) u_regch(
    .clk    ( clk          ),
    .cen    ( cen          ),
    .rst    ( rst          ),
    .din    ( regop_in     ),
    .drop   ( shift_out    )
);

wire up_mult_I    = up_mult   & update_op_I;
wire up_mult_II   = up_mult   & update_op_II;
wire up_mult_IV   = up_mult   & update_op_IV;
wire up_ksl_tl_IV = up_ksl_tl & update_op_IV;
wire up_ar_dr_op  = up_ar_dr  & update_op_I;
wire up_sl_rr_op  = up_sl_rr  & update_op_I;

assign regop_in = { // 4 bytes:
        up_mult_IV  ? din[7]      : shift_out[31], // AM enable
        up_mult_I   ? din[6:5]    : shift_out[30:29], // Vib enable, EG type, KSR
        up_mult_II  ? din[4:0]    : shift_out[28:24], // KSR + Mult

        up_ksl_tl_IV? din         : shift_out[23:16], // KSL + TL

        up_ar_dr_op ? din         : shift_out[15: 8],

        up_sl_rr_op ? din         : shift_out[ 7: 0]
    };

endmodule // jtopl_reg
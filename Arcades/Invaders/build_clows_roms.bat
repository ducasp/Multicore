
set rom_path=roms\

copy /b %rom_path%clowns\h2.cpu + %rom_path%clowns\g2.cpu %rom_path%clowns\clowns.h
copy /b %rom_path%clowns\f2.cpu + %rom_path%clowns\e2.cpu %rom_path%clowns\clowns.g
copy /b %rom_path%clowns\d2.cpu + %rom_path%clowns\c2.cpu %rom_path%clowns\clowns.f
copy /b %rom_path%clowns\dummy2k  %rom_path%clowns\clowns.e

romgen %rom_path%clowns\clowns.E CLOWNS_ROM_E 11 a r > %rom_path%clowns_rom_e.vhd
romgen %rom_path%clowns\clowns.F CLOWNS_ROM_F 11 a r > %rom_path%clowns_rom_f.vhd
romgen %rom_path%clowns\clowns.G CLOWNS_ROM_G 11 a r > %rom_path%clowns_rom_g.vhd
romgen %rom_path%clowns\clowns.H CLOWNS_ROM_H 11 a r > %rom_path%clowns_rom_h.vhd
pause
echo done

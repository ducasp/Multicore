/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Robotron-FPGA MiST top-level
//
//  Robotron-FPGA is Copyright 2012 ShareBrained Technology, Inc.
//
//  Supports:
//  Robotron 2048/Joust/Stargate/Bubbles/Splat/Sinistar
//============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module RobotronFPGA_mc2(
	// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [4:1]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,
		
	// SDRAM	(H57V256)
	output [12:0] SDRAM_A,
	output  [1:0] SDRAM_BA,
	inout  [15:0] SDRAM_DQ,
	output        SDRAM_DQMH,
	output        SDRAM_DQML,
	output        SDRAM_CKE,
	output        SDRAM_nCS,
	output        SDRAM_nWE,
	output        SDRAM_nRAS,
	output        SDRAM_nCAS,
	output        SDRAM_CLK,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  = 1'bz,
	inout wire	ps2_mouse_data_io = 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output        AUDIO_L,
	output        AUDIO_R,
	input wire	ear_i,
	output wire	mic_o					= 1'b0,

		// VGA
	output  [4:0] VGA_R,
	output  [4:0] VGA_G,
	output  [4:0] VGA_B,
	output        VGA_HS,
	output        VGA_VS,

		// HDMI
	output wire	[7:0]tmds_o			= 8'b00000000,

		//STM32
	input wire	stm_tx_i,
	output wire	stm_rx_o,
	output wire	stm_rst_o			= 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
		
	inout wire	stm_b8_io, 
	inout wire	stm_b9_io,

	input         SPI_SCK,
	output        SPI_DO,
	input         SPI_DI,
	input         SPI_SS2
);



//`define CORE_NAME "ROBOTRON"
//`define CORE_NAME "SINISTAR"
//`define CORE_NAME "SPLAT"
//`define CORE_NAME "BUBBLES"
//`define CORE_NAME "SINISTAR"
//`define CORE_NAME "JOUST"

//CORE_NAME now is inside build.id
`include "rtl/build_id.v" 

localparam CONF_STR = {
	"P,CORE_NAME.dat;",
	"S,DAT,Alternative ROM...;",
	"O34,Scanlines,Off,25%,50%,75%;",
	"O5,Blend,Off,On;",
	"O6,Swap Joysticks,Off,On;",
	"T0,Reset;",
	"O7,Patrons list,Off,On;",
	"V,v1.0."
};

wire       rotate    = 0;//status[2];
wire [1:0] scanlines = status[4:3];
wire       blend     = status[5];
wire       joyswap   = status[6];

reg  [7:0] SW;
reg  [7:0] JA;
reg  [7:0] JB;
reg  [3:0] BTN;
reg        blitter_sc2, sinistar;
reg  [1:0] orientation; // [left/right, landscape/portrait]

reg signed [11:0] adj_x;
reg signed [11:0] adj_y;
reg scroll = 0;

always @(*) begin
	orientation = 2'b00;
	SW = 8'h00;
	blitter_sc2 = 0;
	sinistar = 0;
	
	adj_x = -100;
	adj_y = -500;
	scroll = 1;

	if (`CORE_NAME == "ROBOTRON") begin
		BTN = { m_one_player, m_two_players, m_coin1 | m_coin2, reset };
		JA  = ~{ m_right2, m_left2, m_down2, m_up2, m_right, m_left, m_down, m_up };
		JB  = ~{ m_right2, m_left2, m_down2, m_up2, m_right, m_left, m_down, m_up };
		
	end else if (`CORE_NAME == "JOUST") begin
		BTN = { m_two_players, m_one_player, m_coin1 | m_coin2, reset };
		JA  = ~{ 5'b00000, m_fireA, m_right, m_left };
		JB  = ~{ 5'b00000, m_fire2A, m_right2, m_left2 };
		
	end	else if (`CORE_NAME == "SPLAT") begin
		blitter_sc2 = 1;
		BTN = { m_one_player, m_two_players, m_coin1 | m_coin2, reset };
		JA  = ~{ m_right2, m_left2, m_down2, m_up2, m_right, m_left, m_down, m_up };
		JB  = ~{ m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3 };
		
	end else if (`CORE_NAME == "BUBBLES") begin
		BTN = { m_two_players, m_one_player, m_coin1 | m_coin2, reset };
		JA  = ~{ 4'b0000, m_right, m_left, m_down, m_up };
		JB  = ~{ 4'b0000, m_right2, m_left2, m_down2, m_up2 };
		
	end else if (`CORE_NAME == "STARGATE") begin
		BTN = { m_two_players, m_one_player, m_coin1 | m_coin2, reset };
		JA  = ~{ m_fireE, m_up, m_down, m_left | m_right, m_fireD, m_fireC, m_fireB, m_fireA };
		JB  = ~{ m_fire2E, m_up2, m_down2, m_left2 | m_right2, m_fire2D, m_fire2C, m_fire2B, m_fire2A };
		
	end else if (`CORE_NAME == "SINISTAR") begin
		sinistar = 1;
		orientation = 2'b01;
		
		adj_x = 600;
		adj_y = -150;
	
		BTN = { m_two_players, m_one_player, m_coin1 | m_coin2, reset };
		JA  = { sin_x, 2'b00, m_right | m_left | m_right2 | m_left2, sin_y, 2'b00, m_up | m_down | m_up2 | m_down2 };
		JB  = { sin_x, 2'b00, m_right | m_left | m_right2 | m_left2, sin_y, 2'b00, m_up | m_down | m_up2 | m_down2 };
	end
end

//assign LED = ~ioctl_downl;
//assign SDRAM_CLK = clk_mem;
assign SDRAM_CKE = 1;

assign sram_we_n_o		= 1'b1;
assign sram_oe_n_o		= 1'b1;
assign stm_rst_o			= 1'bz;

wire clk_sys, clk_mem, clk_aud;
wire pll_locked;
pll_mist pll(
	.inclk0(clock_50_i),
	.areset(0),
	.c0(clk_mem),//96
	.c1(clk_sys),//12
	.c2(clk_aud),//0.89
	.c3(SDRAM_CLK),
	.locked(pll_locked)
	);

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire  [7:0] audio;
wire        hs, vs;
wire        blankn;
wire  [2:0] r,g;
wire  [1:0] b;
wire        key_pressed;
wire  [7:0] key_code;
wire        key_strobe;

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

/*
ROM Structure:
0000-BFFF main cpu 48k
C000-CFFF snd  cpu  4k
*/
data_io #(
	.STRLEN(($size(CONF_STR)>>3)))
data_io(
	.clk_sys       ( clk_mem      ),
	.SPI_SCK       ( SPI_SCK      ),
	.SPI_SS2       ( SPI_SS2      ),
	.SPI_DI        ( SPI_DI       ),
	.SPI_DO        ( SPI_DO       ),
	
	.data_in		 	( osd_s & keys_s ),
	.conf_str		( CONF_STR ),
	.status			( status ),

	.ioctl_download( ioctl_downl  ),
	.ioctl_index   ( ioctl_index  ),
	.ioctl_wr      ( ioctl_wr     ),
	.ioctl_addr    ( ioctl_addr   ),
	.ioctl_dout    ( ioctl_dout   )
);

reg         port1_req, port2_req;
wire [23:1] mem_addr;
wire [15:0] mem_do;
wire [15:0] mem_di;
wire        mem_oe;
wire        mem_we;
wire        ramcs;
wire        romcs;
wire        ramlb;
wire        ramub;

reg         clkref;
always @(posedge clk_sys) clkref <= ~clkref;

sdram #(.MHZ(96)) sdram(
	.*,
	.init_n        ( pll_locked   ),
	.clk           ( clk_mem      ),
	.clkref        ( clkref       ),

	// ROM upload
	.port1_req     ( port1_req    ),
	.port1_ack     ( ),
	.port1_a       ( ioctl_addr[22:0] ),
	.port1_ds      ( 2'b11 ),
	.port1_we      ( ioctl_downl ),
	.port1_d       ( {ioctl_dout[7:4], ioctl_dout[7:4], ioctl_dout[3:0], ioctl_dout[3:0]} ),
	.port1_q       ( ),

	// CPU/video access
	.cpu1_addr     ( ioctl_downl ? 17'h1ffff : sdram_addr ),
	.cpu1_d        ( mem_di  ),
	.cpu1_q        ( mem_do  ),
    .cpu1_oe       ( ~mem_oe & ~(ramcs & romcs) ),
	.cpu1_we       ( ~mem_we & ~(ramcs & romcs) ),
	.cpu1_ds       ( ~romcs ? 2'b11 : ~{ramub, ramlb} )
);

// ROM address to SDRAM address:
// 0xxx-8xxx -> 0xxx-8xxx
// DXXX-FXXX -> 9xxx-Bxxx
wire [17:1] sdram_addr = ~romcs ? {1'b0, mem_addr[16], ~mem_addr[16] & mem_addr[15], mem_addr[14:1]} : { 1'b1, mem_addr[16:1] };

always @(posedge clk_mem) begin
	reg        ioctl_wr_last = 0;

	ioctl_wr_last <= ioctl_wr;
	if (ioctl_downl) begin
		if (~ioctl_wr_last && ioctl_wr) begin
			port1_req <= ~port1_req;
			port2_req <= ~port2_req;
		end
	end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sys) begin
	reg ioctl_downlD;
	ioctl_downlD <= ioctl_downl;

	if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
	reset <= status[0] | ~btn_n_i[4] | ioctl_downl | ~rom_loaded;
end

robotron_soc robotron_soc (
	.clock       ( clk_sys     ),
	.clock_snd   ( clk_aud     ),
	.vgaRed      ( r           ),
	.vgaGreen    ( g           ),
	.vgaBlue     ( b           ),
	.Hsync       ( hs          ),
	.Vsync       ( vs          ),
	.audio_out   ( audio       ),

	.blitter_sc2 ( blitter_sc2 ),
	.sinistar    ( sinistar    ),
	.BTN         ( BTN         ),
	.SIN_FIRE    ( ~m_fireA & ~m_fire2A ),
	.SIN_BOMB    ( ~m_fireB & ~m_fire2B ),
	.SW          ( SW          ),
	.JA          ( JA          ),
	.JB          ( JB          ),

	.MemAdr      ( mem_addr    ),
	.MemDin      ( mem_di      ),
	.MemDout     ( mem_do      ),
	.MemOE       ( mem_oe      ),
	.MemWR       ( mem_we      ),
	.RamCS       ( ramcs       ),
	.RamLB       ( ramlb       ),
	.RamUB       ( ramub       ),
	.FlashCS     ( romcs       ),

	.dl_clock    ( clk_mem  ),
	.dl_addr     ( ioctl_addr[15:0] ),
	.dl_data     ( ioctl_dout ),
	.dl_wr       ( ioctl_wr )
);

wire [5:0] vga_r_s;	
wire [5:0] vga_g_s;	
wire [5:0] vga_b_s;	

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(11)) mist_video(
	.clk_sys        ( clk_sys          ),
	.SPI_SCK        ( SPI_SCK          ),
	.SPI_SS3        ( SPI_SS2          ),
	.SPI_DI         ( SPI_DI           ),
	.R              ( r                ),
	.G              ( g                ),
	.B              ( {b, b[1] }       ),
	.HSync          ( hs               ),
	.VSync          ( vs               ),
	.VGA_R          ( vga_r_s          ),
	.VGA_G          ( vga_g_s          ),
	.VGA_B          ( vga_b_s          ),
	.VGA_VS         ( VGA_VS           ),
	.VGA_HS         ( VGA_HS           ),
	.rotate         ( orientation ),
	.scandoubler_disable( scandoublerD ),
	.ce_divider     ( 1'b1             ),
	.scanlines      ( scanlines        ),
	.blend          ( blend            ),
	
	.patrons        ( status[7]      ),	
	.PATRON_ADJ_X   ( adj_x ),
	.PATRON_ADJ_Y   ( adj_y ), 
	.PATRON_DOUBLE_WIDTH ( 0 ),
	.PATRON_DOUBLE_HEIGHT ( 0 ),
	.PATRON_SCROLL  ( scroll ),	
	.osd_enable 	 ( osd_enable )
	);

assign VGA_R = vga_r_s[5:1];
assign VGA_G = vga_g_s[5:1];
assign VGA_B = vga_b_s[5:1];


wire dac_o;
assign AUDIO_L = dac_o;
assign AUDIO_R = dac_o;

dac #(
	.C_bits(8))
dac(
	.clk_i(clk_aud),
	.res_n_i(1),
	.dac_i(audio),
	.dac_o(dac_o)
	);

// Sinistar controls
reg sin_x;
reg sin_y;

always @(posedge clk_sys) begin
	if (m_right | m_right2) sin_x <= 0;
	else if (m_left | m_left2) sin_x <= 1;

	if (m_up | m_up2) sin_y <= 0;
	else if (m_down | m_down2) sin_y <= 1;
end

//--------- ROM DATA PUMP ----------------------------------------------------
	
		reg [15:0] power_on_s	= 16'b1111111111111111;
		reg [7:0] osd_s = 8'b11111111;
		
		wire hard_reset = ~pll_locked;
		
		//--start the microcontroller OSD menu after the power on
		always @(posedge clk_sys) 
		begin
		
				if (hard_reset == 1)
					power_on_s = 16'b1111111111111111;
				else if (power_on_s != 0)
				begin
					power_on_s = power_on_s - 1;
					osd_s = 8'b00111111;
				end 
					
				
				if (ioctl_downl == 1 && osd_s == 8'b00111111)
					osd_s = 8'b11111111;
			
		end 

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player = 	~btn_n_i[1] | m_one_player;
wire btn_two_players = 	~btn_n_i[2] | m_two_players;
wire btn_coin  = 			~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD	( 3'b011 )) k_joystick
(
  .clk         	( clk_sys ),
  .kbdint      	( kbd_intr ),
  .kbdscancode 	( kbd_scancode ), 
  
	.joystick_0 	({ joy1_p6_i, joy1_p9_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
	.joystick_1		({ joy2_p6_i, joy2_p9_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
		  
	//-- joystick_0 and joystick_1 should be swapped
	.joyswap 		( 0 ),
		
	//-- player1 and player2 should get both joystick_0 and joystick_1
	.oneplayer		( 1 ),

	//-- tilt, coin4-1, start4-1
	.controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),
		
	//-- fire12-1, up, down, left, right

	.player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
	.player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
		
	//-- keys to the OSD
	.osd_o		   ( keys_s ),
	.osd_enable 	( osd_enable ),
	
	//-- sega joystick
	.sega_clk  		( hs ),
	.sega_strobe	( joyX_p7_o )
		
		
);

endmodule 


quartus_sh -t rtl/build_id.tcl ROBOTRON 
quartus_sh --flow compile RobotronFPGA.qpf
copy output_files\RobotronFPGA.rbf Releases\Robotron.mc2

quartus_sh -t rtl/build_id.tcl JOUST 
quartus_sh --flow compile RobotronFPGA.qpf
copy output_files\RobotronFPGA.rbf Releases\Joust.mc2

quartus_sh -t rtl/build_id.tcl SPLAT 
quartus_sh --flow compile RobotronFPGA.qpf
copy output_files\RobotronFPGA.rbf Releases\Splat.mc2

quartus_sh -t rtl/build_id.tcl BUBBLES 
quartus_sh --flow compile RobotronFPGA.qpf
copy output_files\RobotronFPGA.rbf Releases\Bubbles.mc2

quartus_sh -t rtl/build_id.tcl STARGATE 
quartus_sh --flow compile RobotronFPGA.qpf
copy output_files\RobotronFPGA.rbf Releases\Stargate.mc2

quartus_sh -t rtl/build_id.tcl SINISTAR 
quartus_sh --flow compile RobotronFPGA.qpf
copy output_files\RobotronFPGA.rbf Releases\Sinistar.mc2

cd Releases
7z a -tzip Robotron Robotron.*
7z a -tzip Joust Joust.*
7z a -tzip Splat Splat.*
7z a -tzip Bubbles Bubbles.*
7z a -tzip Stargate Stargate.*
7z a -tzip Sinistar Sinistar.*
cd ..

pause
/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*
  MIT License

  Copyright (c) 2019 Richard Eng

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

/*
  Pong - Horizontal Counter Circuit
  ---------------------------------
*/
`default_nettype none

module hcounter
(
  input wire clk7_159,
  output wire h1, h2, h4, h8, h16, h32, h64, h128, h256, _h256, hreset, _hreset
);

/*
wire f7_to_e7b;

ls93 f8(clk7_159, , hreset, hreset, h1, h2, h4, h8);
ls93 f9(h8, , hreset, hreset, h16, h32, h64, h128);
ls107 f6b(h128, _hreset, 1'b1, 1'b1, h256, _h256);
ls30 f7(h256, 1'b1, 1'b1, 1'b1, h4, h2, h128, h64, f7_to_e7b);
ls74 e7b(clk7_159, f7_to_e7b, 1'b1, 1'b1, _hreset, hreset);
*/

/* verilator lint_off UNOPTFLAT */
reg [8:0] hcnt;
/* verilator lint_on UNOPTFLAT */

initial hcnt = 9'd0;

assign { _h256, h256, h128, h64, h32, h16, h8, h4, h2, h1 } = { ~hcnt[8], hcnt[8], hcnt[7], hcnt[6], hcnt[5], hcnt[4], hcnt[3], hcnt[2], hcnt[1], hcnt[0] };

always @(negedge clk7_159 or posedge hreset) begin
    if (hreset)
        hcnt <= 9'd0;
    else
        hcnt <= hcnt + 1'b1;
end

reg rst;

initial rst = 1'b0;

always @(posedge clk7_159) begin
    rst <= (hcnt == 9'd454);
end

assign hreset = rst;
assign _hreset = ~hreset;

endmodule
